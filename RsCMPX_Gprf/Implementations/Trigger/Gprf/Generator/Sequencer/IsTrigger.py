from typing import List

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ......Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class IsTriggerCls:
	"""IsTrigger commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("isTrigger", core, parent)

	def get_catalog(self) -> List[str]:
		"""SCPI: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog \n
		Snippet: value: List[str] = driver.trigger.gprf.generator.sequencer.isTrigger.get_catalog() \n
		Queries all available trigger source strings. \n
			:return: trigger: Comma-separated list of strings. Each string represents a supported source.
		"""
		response = self._core.io.query_str('TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog?')
		return Conversions.str_to_str_list(response)

	def get_source(self) -> str:
		"""SCPI: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce \n
		Snippet: value: str = driver.trigger.gprf.generator.sequencer.isTrigger.get_source() \n
		Selects a trigger source for triggering sequencer list incrementations. A complete list of all supported strings can be
		queried using method RsCMPX_Gprf.Trigger.Gprf.Generator.Sequencer.IsTrigger.catalog. \n
			:return: trigger: No help available
		"""
		response = self._core.io.query_str('TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce?')
		return trim_str_response(response)

	def set_source(self, trigger: str) -> None:
		"""SCPI: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce \n
		Snippet: driver.trigger.gprf.generator.sequencer.isTrigger.set_source(trigger = 'abc') \n
		Selects a trigger source for triggering sequencer list incrementations. A complete list of all supported strings can be
		queried using method RsCMPX_Gprf.Trigger.Gprf.Generator.Sequencer.IsTrigger.catalog. \n
			:param trigger: No help available
		"""
		param = Conversions.value_to_quoted_str(trigger)
		self._core.io.write(f'TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce {param}')
