from enum import Enum
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_DEFAULT as DefaultRepCap
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_EMPTY as EmptyRepCap


# noinspection SpellCheckingInspection
class Instance(Enum):
	"""Global Repeated capability Instance"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Inst1 = 1
	Inst2 = 2
	Inst3 = 3
	Inst4 = 4
	Inst5 = 5
	Inst6 = 6
	Inst7 = 7
	Inst8 = 8
	Inst9 = 9
	Inst10 = 10
	Inst11 = 11
	Inst12 = 12
	Inst13 = 13
	Inst14 = 14
	Inst15 = 15
	Inst16 = 16
	Inst17 = 17
	Inst18 = 18
	Inst19 = 19
	Inst20 = 20
	Inst21 = 21
	Inst22 = 22
	Inst23 = 23
	Inst24 = 24
	Inst25 = 25
	Inst26 = 26
	Inst27 = 27
	Inst28 = 28
	Inst29 = 29
	Inst30 = 30
	Inst31 = 31
	Inst32 = 32


# noinspection SpellCheckingInspection
class Bench(Enum):
	"""Repeated capability Bench"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20


# noinspection SpellCheckingInspection
class Box(Enum):
	"""Repeated capability Box"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32


# noinspection SpellCheckingInspection
class FrequencySource(Enum):
	"""Repeated capability FrequencySource"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Src1 = 1
	Src2 = 2


# noinspection SpellCheckingInspection
class Index(Enum):
	"""Repeated capability Index"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32


# noinspection SpellCheckingInspection
class LevelSource(Enum):
	"""Repeated capability LevelSource"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Src1 = 1
	Src2 = 2


# noinspection SpellCheckingInspection
class Marker(Enum):
	"""Repeated capability Marker"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class Positioner(Enum):
	"""Repeated capability Positioner"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ix1 = 1
	Ix2 = 2
	Ix3 = 3
	Ix4 = 4
	Ix5 = 5
	Ix6 = 6
	Ix7 = 7
	Ix8 = 8
	Ix9 = 9
	Ix10 = 10
	Ix11 = 11
	Ix12 = 12
	Ix13 = 13
	Ix14 = 14
	Ix15 = 15
	Ix16 = 16
	Ix17 = 17
	Ix18 = 18
	Ix19 = 19
	Ix20 = 20
	Ix21 = 21
	Ix22 = 22
	Ix23 = 23
	Ix24 = 24
	Ix25 = 25
	Ix26 = 26
	Ix27 = 27
	Ix28 = 28
	Ix29 = 29
	Ix30 = 30
	Ix31 = 31
	Ix32 = 32


# noinspection SpellCheckingInspection
class Sensor(Enum):
	"""Repeated capability Sensor"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3


# noinspection SpellCheckingInspection
class Stream(Enum):
	"""Repeated capability Stream"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
