Maximum
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Maximum_Average.rst
	Gprf_Measurement_Spectrum_Maximum_Current.rst
	Gprf_Measurement_Spectrum_Maximum_Maximum.rst
	Gprf_Measurement_Spectrum_Maximum_Minimum.rst