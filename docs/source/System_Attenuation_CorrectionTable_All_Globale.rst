Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SYSTem:ATTenuation:CTABle:ALL:GLOBal

.. code-block:: python

	DELete:SYSTem:ATTenuation:CTABle:ALL:GLOBal



.. autoclass:: RsCMPX_Gprf.Implementations.System.Attenuation.CorrectionTable.All.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: