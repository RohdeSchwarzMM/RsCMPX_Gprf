Dgain
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:DGAin
	single: SOURce:GPRF:GENerator<Instance>:LIST:DGAin:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:DGAin
	SOURce:GPRF:GENerator<Instance>:LIST:DGAin:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Dgain.DgainCls
	:members:
	:undoc-members:
	:noindex: