Move
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.Move.MoveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.positioner.move.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Positioner_Move_To.rst