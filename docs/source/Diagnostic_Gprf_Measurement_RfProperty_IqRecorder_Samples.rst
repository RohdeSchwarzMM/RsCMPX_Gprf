Samples
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:SAMPles

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:SAMPles



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.IqRecorder.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex: