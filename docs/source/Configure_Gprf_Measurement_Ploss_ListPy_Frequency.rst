Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<instance>:PLOSs:LIST:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<instance>:PLOSs:LIST:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: