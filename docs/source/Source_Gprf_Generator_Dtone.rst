Dtone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:DTONe:RATio

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:DTONe:RATio



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.dtone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Dtone_Level.rst
	Source_Gprf_Generator_Dtone_Ofrequency.rst