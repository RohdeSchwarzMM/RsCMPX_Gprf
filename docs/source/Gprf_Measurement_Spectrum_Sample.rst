Sample
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Sample.SampleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.sample.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Sample_Average.rst
	Gprf_Measurement_Spectrum_Sample_Current.rst
	Gprf_Measurement_Spectrum_Sample_Maximum.rst
	Gprf_Measurement_Spectrum_Sample_Minimum.rst