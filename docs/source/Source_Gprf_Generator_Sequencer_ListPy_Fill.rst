Fill
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:SINDex
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:RANGe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:SINDex
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:RANGe



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Fill.FillCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.listPy.fill.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_ListPy_Fill_Apply.rst
	Source_Gprf_Generator_Sequencer_ListPy_Fill_Dgain.rst
	Source_Gprf_Generator_Sequencer_ListPy_Fill_Frequency.rst
	Source_Gprf_Generator_Sequencer_ListPy_Fill_Lrms.rst