Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:ATTenuation:CTABle:INFO[:TENVironment]

.. code-block:: python

	[CONFigure]:SYSTem:ATTenuation:CTABle:INFO[:TENVironment]



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Attenuation.CorrectionTable.Info.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex: