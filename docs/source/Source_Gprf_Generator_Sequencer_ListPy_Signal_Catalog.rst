Catalog
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog:LONG
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog:LONG
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Signal.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: