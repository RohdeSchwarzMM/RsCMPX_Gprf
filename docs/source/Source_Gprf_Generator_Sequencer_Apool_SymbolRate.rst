SymbolRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SRATe:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SRATe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SRATe:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SRATe



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: