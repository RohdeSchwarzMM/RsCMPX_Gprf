Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:OFF

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:OFF



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Signal.All.Off.OffCls
	:members:
	:undoc-members:
	:noindex: