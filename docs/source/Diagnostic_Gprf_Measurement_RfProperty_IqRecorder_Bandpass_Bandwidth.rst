Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:BANDpass:BWIDth

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:BANDpass:BWIDth



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.IqRecorder.Bandpass.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: