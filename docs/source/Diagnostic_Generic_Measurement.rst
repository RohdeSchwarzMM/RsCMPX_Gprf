Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Generic.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.generic.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Generic_Measurement_Dapi.rst