Average
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Average_Average.rst
	Gprf_Measurement_Spectrum_Average_Current.rst
	Gprf_Measurement_Spectrum_Average_Maximum.rst
	Gprf_Measurement_Spectrum_Average_Minimum.rst