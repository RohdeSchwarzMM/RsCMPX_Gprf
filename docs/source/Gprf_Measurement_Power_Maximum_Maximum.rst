Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:MAXimum:MAXimum
	single: READ:GPRF:MEASurement<Instance>:POWer:MAXimum:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:MAXimum:MAXimum
	READ:GPRF:MEASurement<Instance>:POWer:MAXimum:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Maximum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: