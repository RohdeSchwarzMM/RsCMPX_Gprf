Create
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Create.CreateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_System.rst
	Create_Tenvironment.rst