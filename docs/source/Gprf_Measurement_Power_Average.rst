Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:POWer:AVERage
	single: READ:GPRF:MEASurement<Instance>:POWer:AVERage

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:AVERage
	FETCh:GPRF:MEASurement<Instance>:POWer:AVERage
	READ:GPRF:MEASurement<Instance>:POWer:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_Average_Rms.rst