Paratio
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:PARatio:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:PARatio

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:PARatio:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:PARatio



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.Paratio.ParatioCls
	:members:
	:undoc-members:
	:noindex: