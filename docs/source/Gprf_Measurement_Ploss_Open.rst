Open
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<instance>:PLOSs:OPEN

.. code-block:: python

	INITiate:GPRF:MEASurement<instance>:PLOSs:OPEN



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Open.OpenCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.open.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Open_State.rst