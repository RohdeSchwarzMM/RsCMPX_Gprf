MultiEval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:LTEDl:MEASurement<Instance>:MEValuation:SOURce

.. code-block:: python

	TRIGger:LTEDl:MEASurement<Instance>:MEValuation:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.LteDl.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.lteDl.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_LteDl_Measurement_MultiEval_Catalog.rst