Retrigger
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:RETRigger
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:RETRigger:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:RETRigger
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:RETRigger:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqVsSlot.ListPy.Retrigger.RetriggerCls
	:members:
	:undoc-members:
	:noindex: