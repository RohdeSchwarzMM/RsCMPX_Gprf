SymbolRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:SRATe

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:SRATe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: