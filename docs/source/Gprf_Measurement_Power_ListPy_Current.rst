Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:LIST:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: