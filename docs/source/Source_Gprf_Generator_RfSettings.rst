RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:LOFRequency
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:LOLevel
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:DGAin
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:PEPower
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:EATTenuation
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:FREQuency
	single: SOURce:GPRF:GENerator<Instance>:RFSettings:LEVel

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:RFSettings:LOFRequency
	SOURce:GPRF:GENerator<Instance>:RFSettings:LOLevel
	SOURce:GPRF:GENerator<Instance>:RFSettings:DGAin
	SOURce:GPRF:GENerator<Instance>:RFSettings:PEPower
	SOURce:GPRF:GENerator<Instance>:RFSettings:EATTenuation
	SOURce:GPRF:GENerator<Instance>:RFSettings:FREQuency
	SOURce:GPRF:GENerator<Instance>:RFSettings:LEVel



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: