Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NIOT:MEASurement<Instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:NIOT:MEASurement<Instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Niot.Measurement.Prach.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: