CorrectionTable
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tenvironment.spath.correctionTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tenvironment_Spath_CorrectionTable_Rx.rst
	Configure_Tenvironment_Spath_CorrectionTable_Tx.rst