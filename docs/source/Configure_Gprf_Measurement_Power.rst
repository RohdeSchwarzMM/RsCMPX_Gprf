Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:SLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:MLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PDEFset

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:MODE
	CONFigure:GPRF:MEASurement<Instance>:POWer:TOUT
	CONFigure:GPRF:MEASurement<Instance>:POWer:SLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:MLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:REPetition
	CONFigure:GPRF:MEASurement<Instance>:POWer:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:POWer:PDEFset



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Power_Catalog.rst
	Configure_Gprf_Measurement_Power_FilterPy.rst
	Configure_Gprf_Measurement_Power_ListPy.rst
	Configure_Gprf_Measurement_Power_ParameterSetList.rst
	Configure_Gprf_Measurement_Power_Trigger.rst