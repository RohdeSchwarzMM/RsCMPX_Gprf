Scpi
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Scpi.ScpiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.scpi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Scpi_Logging.rst