Ofrequency<FrequencySource>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Src1 .. Src2
	rc = driver.source.gprf.generator.sequencer.dtone.ofrequency.repcap_frequencySource_get()
	driver.source.gprf.generator.sequencer.dtone.ofrequency.repcap_frequencySource_set(repcap.FrequencySource.Src1)



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:DTONe:OFRequency<source>

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:DTONe:OFRequency<source>



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Dtone.Ofrequency.OfrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.dtone.ofrequency.clone()