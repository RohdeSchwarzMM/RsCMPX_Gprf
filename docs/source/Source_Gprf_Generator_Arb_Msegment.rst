Msegment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:NAME
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:POFFset
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:PAR
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:DURation
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:SAMPles
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:CRATe
	single: SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:NUMBer

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:NAME
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:POFFset
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:PAR
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:DURation
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:SAMPles
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:CRATe
	SOURce:GPRF:GENerator<Instance>:ARB:MSEGment:NUMBer



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Arb.Msegment.MsegmentCls
	:members:
	:undoc-members:
	:noindex: