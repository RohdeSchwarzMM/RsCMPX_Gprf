Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:TYPE

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:TYPE



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Generator_Spath.rst