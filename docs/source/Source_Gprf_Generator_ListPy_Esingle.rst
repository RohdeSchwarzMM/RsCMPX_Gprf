Esingle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:ESINgle

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:ESINgle



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Esingle.EsingleCls
	:members:
	:undoc-members:
	:noindex: