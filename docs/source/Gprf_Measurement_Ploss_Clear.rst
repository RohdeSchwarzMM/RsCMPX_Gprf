Clear
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:PLOSs:CLEar

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:PLOSs:CLEar



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Clear.ClearCls
	:members:
	:undoc-members:
	:noindex: