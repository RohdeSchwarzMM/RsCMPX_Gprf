Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SYSTem:ATTenuation:CTABle[:TENVironment]

.. code-block:: python

	CREate:SYSTem:ATTenuation:CTABle[:TENVironment]



.. autoclass:: RsCMPX_Gprf.Implementations.Create.System.Attenuation.CorrectionTable.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex: