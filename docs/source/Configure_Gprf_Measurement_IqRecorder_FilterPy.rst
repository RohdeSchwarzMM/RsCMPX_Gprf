FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.iqRecorder.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_IqRecorder_FilterPy_Bandpass.rst
	Configure_Gprf_Measurement_IqRecorder_FilterPy_Gauss.rst