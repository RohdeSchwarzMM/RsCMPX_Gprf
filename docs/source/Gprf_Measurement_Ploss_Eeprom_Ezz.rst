Ezz
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EZZ

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EZZ



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Eeprom.Ezz.EzzCls
	:members:
	:undoc-members:
	:noindex: