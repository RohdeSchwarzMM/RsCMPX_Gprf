Add
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Add.AddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_System.rst
	Add_Tenvironment.rst