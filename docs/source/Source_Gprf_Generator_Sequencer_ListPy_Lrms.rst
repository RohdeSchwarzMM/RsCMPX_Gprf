Lrms
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LRMS
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LRMS:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LRMS
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LRMS:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Lrms.LrmsCls
	:members:
	:undoc-members:
	:noindex: