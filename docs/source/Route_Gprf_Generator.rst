Generator
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.gprf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Gprf_Generator_RfSettings.rst
	Route_Gprf_Generator_Spath.rst