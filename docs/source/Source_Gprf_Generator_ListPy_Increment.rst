Increment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:INCRement:CATalog
	single: SOURce:GPRF:GENerator<Instance>:LIST:INCRement

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:INCRement:CATalog
	SOURce:GPRF:GENerator<Instance>:LIST:INCRement



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Increment.IncrementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.listPy.increment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_ListPy_Increment_Enabling.rst