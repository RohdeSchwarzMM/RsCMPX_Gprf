IqRecorder
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:SRATe
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:BYPass
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:USER
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:INIFile

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:SRATe
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MODE
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:BYPass
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:USER
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:INIFile



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_IqRecorder_Capture.rst
	Configure_Gprf_Measurement_IqRecorder_FilterPy.rst
	Configure_Gprf_Measurement_IqRecorder_IqSettings.rst
	Configure_Gprf_Measurement_IqRecorder_ListPy.rst
	Configure_Gprf_Measurement_IqRecorder_Trigger.rst