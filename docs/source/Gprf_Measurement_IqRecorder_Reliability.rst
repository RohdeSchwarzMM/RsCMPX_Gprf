Reliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:RELiability

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:RELiability



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqRecorder.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: