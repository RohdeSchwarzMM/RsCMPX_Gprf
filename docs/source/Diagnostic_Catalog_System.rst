System
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Catalog.System.SystemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.catalog.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Catalog_System_Connectors.rst