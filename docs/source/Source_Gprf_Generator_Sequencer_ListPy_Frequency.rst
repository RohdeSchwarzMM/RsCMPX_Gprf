Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FREQuency
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FREQuency:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FREQuency
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FREQuency:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: