Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:NRDL:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:NRDL:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:NRDL:MEASurement<Instance>:SPATh:COUNt
	ROUTe:NRDL:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrDl.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: