Scenario
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Measurement.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.gprf.measurement.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Gprf_Measurement_Scenario_Catalog.rst