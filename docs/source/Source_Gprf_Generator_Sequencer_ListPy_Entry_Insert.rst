Insert
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:INSert

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:INSert



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Entry.Insert.InsertCls
	:members:
	:undoc-members:
	:noindex: