Ambient
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:BASE:TEMPerature:OPERating:AMBient

.. code-block:: python

	SENSe:BASE:TEMPerature:OPERating:AMBient



.. autoclass:: RsCMPX_Gprf.Implementations.Sense.Base.Temperature.Operating.Ambient.AmbientCls
	:members:
	:undoc-members:
	:noindex: