Rlist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:RLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:RLISt



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Rlist.RlistCls
	:members:
	:undoc-members:
	:noindex: