Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_FftSpecAn.rst
	Trigger_Gprf_Measurement_IqRecorder.rst
	Trigger_Gprf_Measurement_IqVsSlot.rst
	Trigger_Gprf_Measurement_Power.rst
	Trigger_Gprf_Measurement_Spectrum.rst