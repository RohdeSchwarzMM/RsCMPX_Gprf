CumulativeDistribFnc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.CumulativeDistribFnc.CumulativeDistribFncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.cumulativeDistribFnc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_CumulativeDistribFnc_Power.rst
	Gprf_Measurement_Power_CumulativeDistribFnc_Probability.rst
	Gprf_Measurement_Power_CumulativeDistribFnc_Sample.rst