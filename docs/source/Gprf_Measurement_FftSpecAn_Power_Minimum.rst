Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MINimum
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MINimum
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: