Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Gprf_Measurement_Ploss.rst
	Catalog_Gprf_Measurement_Spath.rst