Level<LevelSource>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Src1 .. Src2
	rc = driver.source.gprf.generator.dtone.level.repcap_levelSource_get()
	driver.source.gprf.generator.dtone.level.repcap_levelSource_set(repcap.LevelSource.Src1)



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:DTONe:LEVel<source>

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:DTONe:LEVel<source>



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Dtone.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.dtone.level.clone()