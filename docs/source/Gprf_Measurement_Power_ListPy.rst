ListPy
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_ListPy_Average.rst
	Gprf_Measurement_Power_ListPy_Current.rst
	Gprf_Measurement_Power_ListPy_Maximum.rst
	Gprf_Measurement_Power_ListPy_Minimum.rst
	Gprf_Measurement_Power_ListPy_Peak.rst
	Gprf_Measurement_Power_ListPy_StandardDev.rst