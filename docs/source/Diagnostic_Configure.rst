Configure
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_Gprf.rst
	Diagnostic_Configure_System.rst