Sensor<Sensor>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.gprf.measurement.nrpm.sensor.repcap_sensor_get()
	driver.configure.gprf.measurement.nrpm.sensor.repcap_sensor_set(repcap.Sensor.Nr1)





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Nrpm.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.nrpm.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Nrpm_Sensor_Frequency.rst