Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:GPRF:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:SPATh:COUNt
	ROUTe:GPRF:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: