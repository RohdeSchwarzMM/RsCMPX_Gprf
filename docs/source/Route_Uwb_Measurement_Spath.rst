Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:UWB:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:UWB:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:UWB:MEASurement<Instance>:SPATh:COUNt
	ROUTe:UWB:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Uwb.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: