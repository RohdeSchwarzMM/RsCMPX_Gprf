Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:TIME:SOURce
	single: SYSTem:TIME:NTP

.. code-block:: python

	SYSTem:TIME:SOURce
	SYSTem:TIME:NTP



.. autoclass:: RsCMPX_Gprf.Implementations.System.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Time_Local.rst