Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:CURRent

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:CURRent
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Peaks.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: