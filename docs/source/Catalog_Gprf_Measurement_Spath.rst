Spath<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.catalog.gprf.measurement.spath.repcap_stream_get()
	driver.catalog.gprf.measurement.spath.repcap_stream_set(repcap.Stream.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CATalog:GPRF:MEASurement<Instance>:SPATh<StreamNumber>

.. code-block:: python

	CATalog:GPRF:MEASurement<Instance>:SPATh<StreamNumber>



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Gprf.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.gprf.measurement.spath.clone()