State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.State.StateCls
	:members:
	:undoc-members:
	:noindex: