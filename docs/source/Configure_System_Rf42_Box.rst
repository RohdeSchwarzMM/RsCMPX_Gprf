Box<Box>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.configure.system.rf42.box.repcap_box_get()
	driver.configure.system.rf42.box.repcap_box_set(repcap.Box.Nr1)





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rf42.Box.BoxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.rf42.box.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Rf42_Box_Apreset.rst