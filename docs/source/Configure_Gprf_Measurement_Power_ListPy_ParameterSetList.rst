ParameterSetList
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:PSET
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:PSET:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:PSET
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:PSET:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.ParameterSetList.ParameterSetListCls
	:members:
	:undoc-members:
	:noindex: