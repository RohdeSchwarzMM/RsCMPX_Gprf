NrSub
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.NrSub.NrSubCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.nrSub.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_NrSub_Measurement.rst