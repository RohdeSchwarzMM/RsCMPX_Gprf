Peaks
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Peaks.PeaksCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.fftSpecAn.peaks.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_FftSpecAn_Peaks_Average.rst
	Gprf_Measurement_FftSpecAn_Peaks_Current.rst