Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MANual:EXECute



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.Segments.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: