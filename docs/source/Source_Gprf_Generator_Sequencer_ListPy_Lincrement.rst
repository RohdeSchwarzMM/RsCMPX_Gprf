Lincrement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LINCrement
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LINCrement:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LINCrement
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:LINCrement:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Lincrement.LincrementCls
	:members:
	:undoc-members:
	:noindex: