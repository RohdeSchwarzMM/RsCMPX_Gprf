State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:SHORt:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:SHORt:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Short.State.StateCls
	:members:
	:undoc-members:
	:noindex: