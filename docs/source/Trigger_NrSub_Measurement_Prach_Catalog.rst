Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.Measurement.Prach.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: