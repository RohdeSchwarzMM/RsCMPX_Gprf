Dapi
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.DapiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.dapi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Dapi_Logging.rst