Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SYSTem:ATTenuation:CTABle[:TENVironment]

.. code-block:: python

	DELete:SYSTem:ATTenuation:CTABle[:TENVironment]



.. autoclass:: RsCMPX_Gprf.Implementations.System.Attenuation.CorrectionTable.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex: