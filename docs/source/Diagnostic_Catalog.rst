Catalog
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.catalog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Catalog_System.rst