Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	CREate:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Gprf.Implementations.Create.System.Attenuation.CorrectionTable.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: