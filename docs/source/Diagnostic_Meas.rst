Meas
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Meas.MeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.meas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Meas_Scpi.rst