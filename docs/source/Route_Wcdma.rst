Wcdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Wcdma_Measurement.rst