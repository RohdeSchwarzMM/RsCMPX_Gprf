Dtone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:DTONe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:DTONe



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Signal.All.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex: