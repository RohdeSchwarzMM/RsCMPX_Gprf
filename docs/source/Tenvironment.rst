Tenvironment
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tenvironment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Tenvironment_Spath.rst