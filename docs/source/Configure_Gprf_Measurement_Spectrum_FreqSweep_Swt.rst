Swt
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.FreqSweep.Swt.SwtCls
	:members:
	:undoc-members:
	:noindex: