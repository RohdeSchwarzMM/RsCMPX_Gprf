Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Minimum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: