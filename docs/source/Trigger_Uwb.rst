Uwb
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Uwb.UwbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.uwb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Uwb_Measurement.rst