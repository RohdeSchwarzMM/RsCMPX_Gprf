Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:NRSub:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:SPATh:COUNt
	ROUTe:NRSub:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrSub.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: