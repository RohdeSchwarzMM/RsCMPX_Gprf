Debug
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Trigger.Add.Debug.DebugCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.trigger.add.debug.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Trigger_Add_Debug_Output.rst