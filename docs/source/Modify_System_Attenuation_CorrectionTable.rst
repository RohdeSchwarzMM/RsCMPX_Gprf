CorrectionTable
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Modify.System.Attenuation.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.modify.system.attenuation.correctionTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Modify_System_Attenuation_CorrectionTable_Globale.rst
	Modify_System_Attenuation_CorrectionTable_Tenvironment.rst