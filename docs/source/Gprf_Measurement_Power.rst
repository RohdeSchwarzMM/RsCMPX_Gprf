Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:POWer
	single: STOP:GPRF:MEASurement<Instance>:POWer
	single: ABORt:GPRF:MEASurement<Instance>:POWer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:POWer
	STOP:GPRF:MEASurement<Instance>:POWer
	ABORt:GPRF:MEASurement<Instance>:POWer



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_AmplitudeProbDensity.rst
	Gprf_Measurement_Power_Average.rst
	Gprf_Measurement_Power_CumulativeDistribFnc.rst
	Gprf_Measurement_Power_Current.rst
	Gprf_Measurement_Power_ElapsedStats.rst
	Gprf_Measurement_Power_IqData.rst
	Gprf_Measurement_Power_IqInfo.rst
	Gprf_Measurement_Power_ListPy.rst
	Gprf_Measurement_Power_Maximum.rst
	Gprf_Measurement_Power_Minimum.rst
	Gprf_Measurement_Power_Peak.rst
	Gprf_Measurement_Power_StandardDev.rst
	Gprf_Measurement_Power_State.rst