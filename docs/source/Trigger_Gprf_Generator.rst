Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:TOUT

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:TOUT



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Arb.rst
	Trigger_Gprf_Generator_Sequencer.rst