Minimum
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Minimum_Average.rst
	Gprf_Measurement_Spectrum_Minimum_Current.rst
	Gprf_Measurement_Spectrum_Minimum_Maximum.rst
	Gprf_Measurement_Spectrum_Minimum_Minimum.rst