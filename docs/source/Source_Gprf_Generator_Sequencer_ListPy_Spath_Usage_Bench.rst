Bench<Bench>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.source.gprf.generator.sequencer.listPy.spath.usage.bench.repcap_bench_get()
	driver.source.gprf.generator.sequencer.listPy.spath.usage.bench.repcap_bench_set(repcap.Bench.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SPATh:USAGe:BENCh<nr>

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SPATh:USAGe:BENCh<nr>



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Spath.Usage.Bench.BenchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.listPy.spath.usage.bench.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_ListPy_Spath_Usage_Bench_Tx.rst