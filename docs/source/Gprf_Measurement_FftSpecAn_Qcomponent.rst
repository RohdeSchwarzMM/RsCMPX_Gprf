Qcomponent
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:Q
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:Q

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:Q
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:Q



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Qcomponent.QcomponentCls
	:members:
	:undoc-members:
	:noindex: