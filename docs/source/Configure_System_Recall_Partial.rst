Partial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:RECall:PARTial

.. code-block:: python

	[CONFigure]:SYSTem:RECall:PARTial



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Recall.Partial.PartialCls
	:members:
	:undoc-members:
	:noindex: