Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Canalyzer.rst
	Configure_Gprf_Measurement_Correction.rst
	Configure_Gprf_Measurement_ExtPwrSensor.rst
	Configure_Gprf_Measurement_FftSpecAn.rst
	Configure_Gprf_Measurement_IqRecorder.rst
	Configure_Gprf_Measurement_IqVsSlot.rst
	Configure_Gprf_Measurement_Nrpm.rst
	Configure_Gprf_Measurement_Ploss.rst
	Configure_Gprf_Measurement_Power.rst
	Configure_Gprf_Measurement_RfSettings.rst
	Configure_Gprf_Measurement_Scenario.rst
	Configure_Gprf_Measurement_Spectrum.rst