Mlength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:MLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:MLENgth:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:MLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:MLENgth:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.Mlength.MlengthCls
	:members:
	:undoc-members:
	:noindex: