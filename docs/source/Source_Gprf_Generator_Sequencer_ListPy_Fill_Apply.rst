Apply
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:APPLy

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:APPLy



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Fill.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: