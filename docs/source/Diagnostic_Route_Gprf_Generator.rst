Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTe:GPRF:GENerator<Instance>:SPATh

.. code-block:: python

	DIAGnostic:ROUTe:GPRF:GENerator<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Route.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex: