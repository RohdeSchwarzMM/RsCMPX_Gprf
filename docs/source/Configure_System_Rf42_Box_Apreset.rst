Apreset
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rf42.Box.Apreset.ApresetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.rf42.box.apreset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Rf42_Box_Apreset_Rx.rst
	Configure_System_Rf42_Box_Apreset_Tx.rst