Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Average.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: