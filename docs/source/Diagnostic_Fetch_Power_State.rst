State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FETCh:POWer:STATe

.. code-block:: python

	DIAGnostic:FETCh:POWer:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Fetch.Power.State.StateCls
	:members:
	:undoc-members:
	:noindex: