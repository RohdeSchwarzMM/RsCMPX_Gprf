Partial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:SAVE:PARTial

.. code-block:: python

	[CONFigure]:SYSTem:SAVE:PARTial



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Save.Partial.PartialCls
	:members:
	:undoc-members:
	:noindex: