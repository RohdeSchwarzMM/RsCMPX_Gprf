Dtime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DTIMe
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DTIMe:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DTIMe
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DTIMe:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Dtime.DtimeCls
	:members:
	:undoc-members:
	:noindex: