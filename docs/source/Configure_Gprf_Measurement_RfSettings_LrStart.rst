LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: