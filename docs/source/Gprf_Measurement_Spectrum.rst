Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:SPECtrum
	single: STOP:GPRF:MEASurement<Instance>:SPECtrum
	single: ABORt:GPRF:MEASurement<Instance>:SPECtrum

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:SPECtrum
	STOP:GPRF:MEASurement<Instance>:SPECtrum
	ABORt:GPRF:MEASurement<Instance>:SPECtrum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Average.rst
	Gprf_Measurement_Spectrum_Marker.rst
	Gprf_Measurement_Spectrum_Maximum.rst
	Gprf_Measurement_Spectrum_Minimum.rst
	Gprf_Measurement_Spectrum_ReferenceMarker.rst
	Gprf_Measurement_Spectrum_Rms.rst
	Gprf_Measurement_Spectrum_Sample.rst
	Gprf_Measurement_Spectrum_State.rst