Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:BLUetooth:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:BLUetooth:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:BLUetooth:MEASurement<Instance>:SPATh:COUNt
	ROUTe:BLUetooth:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Bluetooth.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: