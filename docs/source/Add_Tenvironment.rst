Tenvironment
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Add.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.tenvironment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_Tenvironment_Spath.rst