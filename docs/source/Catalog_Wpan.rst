Wpan
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Wpan.WpanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.wpan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Wpan_Measurement.rst