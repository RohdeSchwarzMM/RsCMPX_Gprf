Gauss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:GAUSs:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:GAUSs:BWIDth



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.FilterPy.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex: