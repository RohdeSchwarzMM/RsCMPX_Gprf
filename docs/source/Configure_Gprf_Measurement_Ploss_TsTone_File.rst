File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:OPEN
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:SHORt
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:MATCh

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:OPEN
	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:SHORt
	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:FILE:MATCh



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.TsTone.File.FileCls
	:members:
	:undoc-members:
	:noindex: