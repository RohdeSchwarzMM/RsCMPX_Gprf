Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:TENVironment:SPATh

.. code-block:: python

	CATalog:TENVironment:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.tenvironment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Tenvironment_Connectors.rst