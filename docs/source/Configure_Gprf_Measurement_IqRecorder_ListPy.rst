ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:COUNt
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:STARt
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:STOP
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SLENgth
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:COUNt
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:STARt
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:STOP
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.iqRecorder.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_IqRecorder_ListPy_EnvelopePower.rst
	Configure_Gprf_Measurement_IqRecorder_ListPy_Frequency.rst
	Configure_Gprf_Measurement_IqRecorder_ListPy_Sstop.rst