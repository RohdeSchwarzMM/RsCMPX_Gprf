IsTrigger
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog
	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Sequencer.IsTrigger.IsTriggerCls
	:members:
	:undoc-members:
	:noindex: