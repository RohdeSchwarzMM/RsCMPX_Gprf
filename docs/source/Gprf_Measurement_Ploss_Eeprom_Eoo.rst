Eoo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EOO

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EOO



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Eeprom.Eoo.EooCls
	:members:
	:undoc-members:
	:noindex: