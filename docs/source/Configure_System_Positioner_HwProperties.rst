HwProperties
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:POSitioner<PositionerIdx>:HWPRoperties

.. code-block:: python

	[CONFigure]:SYSTem:POSitioner<PositionerIdx>:HWPRoperties



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.HwProperties.HwPropertiesCls
	:members:
	:undoc-members:
	:noindex: