Power
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Results.Gprf.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.results.gprf.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Results_Gprf_Measurement_Power_Current.rst