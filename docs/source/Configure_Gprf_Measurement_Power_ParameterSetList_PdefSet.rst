PdefSet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:PDEFset

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:PDEFset



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.PdefSet.PdefSetCls
	:members:
	:undoc-members:
	:noindex: