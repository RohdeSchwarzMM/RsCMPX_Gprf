Spath
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Remove.Tenvironment.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.remove.tenvironment.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Remove_Tenvironment_Spath_CorrectionTable.rst