Npeak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MARKer<MarkerNo>:NPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MARKer<MarkerNo>:NPEak



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Marker.Npeak.NpeakCls
	:members:
	:undoc-members:
	:noindex: