NrMmw
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.NrMmw.NrMmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.nrMmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_NrMmw_Measurement.rst