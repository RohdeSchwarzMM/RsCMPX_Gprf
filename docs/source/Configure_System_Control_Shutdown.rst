Shutdown
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:CONTrol:SHUTdown

.. code-block:: python

	[CONFigure]:SYSTem:CONTrol:SHUTdown



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Control.Shutdown.ShutdownCls
	:members:
	:undoc-members:
	:noindex: