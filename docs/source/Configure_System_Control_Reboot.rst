Reboot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:CONTrol:REBoot

.. code-block:: python

	[CONFigure]:SYSTem:CONTrol:REBoot



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Control.Reboot.RebootCls
	:members:
	:undoc-members:
	:noindex: