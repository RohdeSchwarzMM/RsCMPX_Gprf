FreqError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:FERRor
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:FERRor

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:FERRor
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:FERRor



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: