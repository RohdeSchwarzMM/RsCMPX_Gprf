Span
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN:MODE
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:SPAN



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.Frequency.Span.SpanCls
	:members:
	:undoc-members:
	:noindex: