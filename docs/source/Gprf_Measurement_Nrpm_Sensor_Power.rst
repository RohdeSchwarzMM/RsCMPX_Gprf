Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer
	single: FETCh:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer
	single: CALCulate:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer
	FETCh:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer
	CALCulate:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:POWer



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Nrpm.Sensor.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: