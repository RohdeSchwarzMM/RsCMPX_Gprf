FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:TYPE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:TYPE



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.power.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Power_FilterPy_Bandpass.rst
	Configure_Gprf_Measurement_Power_FilterPy_Gauss.rst