Route
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Route.RouteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Route_Gprf.rst
	Diagnostic_Route_NrMmw.rst
	Diagnostic_Route_Uwb.rst