Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FREQuency:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FREQuency:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: