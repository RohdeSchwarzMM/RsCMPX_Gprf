Ezo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EZO

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:EZO



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Eeprom.Ezo.EzoCls
	:members:
	:undoc-members:
	:noindex: