Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:LTE:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:LTE:MEASurement<Instance>:SPATh:COUNt
	ROUTe:LTE:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Lte.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: