Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:CATalog:PDEFset

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:CATalog:PDEFset



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: