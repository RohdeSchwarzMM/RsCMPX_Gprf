Peak
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_Peak_Maximum.rst
	Gprf_Measurement_Power_Peak_Minimum.rst