Mars
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.Mars.MarsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.dapi.logging.mars.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Dapi_Logging_Mars_Psub.rst
	Diagnostic_Configure_System_Dapi_Logging_Mars_Rpc.rst