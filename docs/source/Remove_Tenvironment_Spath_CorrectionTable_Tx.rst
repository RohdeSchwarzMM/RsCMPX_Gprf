Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: REMove:TENVironment:SPATh:CTABle:TX

.. code-block:: python

	REMove:TENVironment:SPATh:CTABle:TX



.. autoclass:: RsCMPX_Gprf.Implementations.Remove.Tenvironment.Spath.CorrectionTable.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: