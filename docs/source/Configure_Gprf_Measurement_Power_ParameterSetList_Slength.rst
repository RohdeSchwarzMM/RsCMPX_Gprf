Slength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:SLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:SLENgth:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:SLENgth
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:SLENgth:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.Slength.SlengthCls
	:members:
	:undoc-members:
	:noindex: