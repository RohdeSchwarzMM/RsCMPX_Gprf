Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SCENario[:ACTivate]

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SCENario[:ACTivate]



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex: