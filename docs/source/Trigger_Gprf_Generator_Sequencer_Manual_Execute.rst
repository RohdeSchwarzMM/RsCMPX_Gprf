Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:MANual:EXECute



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Sequencer.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: