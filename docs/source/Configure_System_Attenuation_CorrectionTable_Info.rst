Info
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Attenuation.CorrectionTable.Info.InfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.attenuation.correctionTable.info.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Attenuation_CorrectionTable_Info_Globale.rst
	Configure_System_Attenuation_CorrectionTable_Info_Tenvironment.rst