Moving
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:POSitioner<PositionerIdx>:MOVing:STOP

.. code-block:: python

	[CONFigure]:SYSTem:POSitioner<PositionerIdx>:MOVing:STOP



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.Moving.MovingCls
	:members:
	:undoc-members:
	:noindex: