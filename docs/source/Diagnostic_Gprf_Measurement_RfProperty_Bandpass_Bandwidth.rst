Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:BANDpass:BWIDth

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:BANDpass:BWIDth



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.Bandpass.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: