Power
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.fftSpecAn.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_FftSpecAn_Power_Average.rst
	Gprf_Measurement_FftSpecAn_Power_Current.rst
	Gprf_Measurement_FftSpecAn_Power_Maximum.rst
	Gprf_Measurement_FftSpecAn_Power_Minimum.rst