RsCMPX_Gprf API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCMPX_Gprf('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCMPX_Gprf.RsCMPX_Gprf
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Add.rst
	Calibration.rst
	Catalog.rst
	Configure.rst
	Create.rst
	Diagnostic.rst
	Gprf.rst
	Modify.rst
	Remove.rst
	Results.rst
	Route.rst
	Sense.rst
	Source.rst
	System.rst
	Tenvironment.rst
	Trigger.rst