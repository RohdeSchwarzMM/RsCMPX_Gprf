RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst32
	# All values (32x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32

Bench
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Bench.Nr1
	# Range:
	Nr1 .. Nr20
	# All values (20x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20

Box
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Box.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

FrequencySource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.FrequencySource.Src1
	# Values (2x):
	Src1 | Src2

Index
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Index.Ix1
	# Range:
	Ix1 .. Ix32
	# All values (32x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10 | Ix11 | Ix12 | Ix13 | Ix14 | Ix15 | Ix16
	Ix17 | Ix18 | Ix19 | Ix20 | Ix21 | Ix22 | Ix23 | Ix24
	Ix25 | Ix26 | Ix27 | Ix28 | Ix29 | Ix30 | Ix31 | Ix32

LevelSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.LevelSource.Src1
	# Values (2x):
	Src1 | Src2

Marker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Marker.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Positioner
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Positioner.Ix1
	# Range:
	Ix1 .. Ix32
	# All values (32x):
	Ix1 | Ix2 | Ix3 | Ix4 | Ix5 | Ix6 | Ix7 | Ix8
	Ix9 | Ix10 | Ix11 | Ix12 | Ix13 | Ix14 | Ix15 | Ix16
	Ix17 | Ix18 | Ix19 | Ix20 | Ix21 | Ix22 | Ix23 | Ix24
	Ix25 | Ix26 | Ix27 | Ix28 | Ix29 | Ix30 | Ix31 | Ix32

Sensor
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Sensor.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

Stream
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Stream.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

