Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Average.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: