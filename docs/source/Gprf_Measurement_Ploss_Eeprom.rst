Eeprom
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Eeprom.EepromCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.eeprom.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Eeprom_Eoo.rst
	Gprf_Measurement_Ploss_Eeprom_Ezo.rst
	Gprf_Measurement_Ploss_Eeprom_Ezz.rst
	Gprf_Measurement_Ploss_Eeprom_Frequency.rst