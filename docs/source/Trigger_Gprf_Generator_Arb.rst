Arb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:DELay
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SLOPe
	single: TRIGger:GPRF:GENerator<Instance>:ARB:RETRigger
	single: TRIGger:GPRF:GENerator<Instance>:ARB:AUTostart
	single: TRIGger:GPRF:GENerator<Instance>[:ARB]:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:DELay
	TRIGger:GPRF:GENerator<Instance>:ARB:SLOPe
	TRIGger:GPRF:GENerator<Instance>:ARB:RETRigger
	TRIGger:GPRF:GENerator<Instance>:ARB:AUTostart
	TRIGger:GPRF:GENerator<Instance>[:ARB]:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Arb_Catalog.rst
	Trigger_Gprf_Generator_Arb_Manual.rst
	Trigger_Gprf_Generator_Arb_Segments.rst