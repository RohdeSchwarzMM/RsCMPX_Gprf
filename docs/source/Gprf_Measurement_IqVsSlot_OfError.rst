OfError
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:IQVSlot:OFERror
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:OFERror
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:OFERror

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:IQVSlot:OFERror
	READ:GPRF:MEASurement<Instance>:IQVSlot:OFERror
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:OFERror



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.OfError.OfErrorCls
	:members:
	:undoc-members:
	:noindex: