Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrDl.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrDl.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrDl_Measurement_Spath.rst