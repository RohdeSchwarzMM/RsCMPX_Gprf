Vbw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.FreqSweep.Vbw.VbwCls
	:members:
	:undoc-members:
	:noindex: