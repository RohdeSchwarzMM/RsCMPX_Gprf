Gprf
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.GprfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.gprf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Gprf_Generator.rst
	Route_Gprf_Measurement.rst