Match
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<instance>:PLOSs:MATCh

.. code-block:: python

	INITiate:GPRF:MEASurement<instance>:PLOSs:MATCh



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Match.MatchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.match.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Match_State.rst