Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:RESults:GPRF:MEASurement<Instance>:POWer:CURRent

.. code-block:: python

	FETCh:RESults:GPRF:MEASurement<Instance>:POWer:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Results.Gprf.Measurement.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: