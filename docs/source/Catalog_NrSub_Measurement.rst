Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.NrSub.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.nrSub.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_NrSub_Measurement_Spath.rst