Minimum
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_Minimum_Current.rst
	Gprf_Measurement_Power_Minimum_Minimum.rst