Canalyzer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:MNAMe
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SEGMent
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:STEP

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:MNAMe
	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SEGMent
	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:STEP



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Canalyzer.CanalyzerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.canalyzer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Canalyzer_IqFile.rst
	Configure_Gprf_Measurement_Canalyzer_Sall.rst