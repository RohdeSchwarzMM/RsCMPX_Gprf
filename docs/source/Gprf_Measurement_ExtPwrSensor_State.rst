State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.ExtPwrSensor.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.extPwrSensor.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_ExtPwrSensor_State_All.rst