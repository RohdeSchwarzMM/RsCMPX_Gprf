RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:LOFRequency
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:LOLevel
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:MLOFfset
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRINterval

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:RFSettings:LOFRequency
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:LOLevel
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:MLOFfset
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRINterval



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_RfSettings_LrStart.rst