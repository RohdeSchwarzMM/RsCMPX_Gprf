Correction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:CORR

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:CORR



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex: