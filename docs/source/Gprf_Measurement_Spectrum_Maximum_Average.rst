Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Maximum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: