Correction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CORR

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CORR



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex: