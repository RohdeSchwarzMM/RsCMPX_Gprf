State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:CANalyzer:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:CANalyzer:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Canalyzer.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.canalyzer.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Canalyzer_State_All.rst