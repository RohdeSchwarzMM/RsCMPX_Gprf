Positioner<Positioner>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix32
	rc = driver.sense.system.positioner.repcap_positioner_get()
	driver.sense.system.positioner.repcap_positioner_set(repcap.Positioner.Ix1)





.. autoclass:: RsCMPX_Gprf.Implementations.Sense.System.Positioner.PositionerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.system.positioner.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_System_Positioner_IsMoving.rst
	Sense_System_Positioner_Position.rst