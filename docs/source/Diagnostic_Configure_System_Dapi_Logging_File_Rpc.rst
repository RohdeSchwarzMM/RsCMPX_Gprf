Rpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:PAYLoad
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:PAYLoad
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.File.Rpc.RpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.dapi.logging.file.rpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Dapi_Logging_File_Rpc_FilterPy.rst