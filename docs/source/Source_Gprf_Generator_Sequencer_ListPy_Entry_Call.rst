Call
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:CALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:CALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Entry.Call.CallCls
	:members:
	:undoc-members:
	:noindex: