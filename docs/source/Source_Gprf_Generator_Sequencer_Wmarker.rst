Wmarker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.source.gprf.generator.sequencer.wmarker.repcap_marker_get()
	driver.source.gprf.generator.sequencer.wmarker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Wmarker.WmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.wmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_Wmarker_Delay.rst