State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRPM:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRPM:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Nrpm.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.nrpm.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Nrpm_State_All.rst