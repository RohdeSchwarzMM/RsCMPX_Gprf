Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdma.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Wcdma_Measurement_MultiEval.rst
	Trigger_Wcdma_Measurement_OlpControl.rst
	Trigger_Wcdma_Measurement_OoSync.rst
	Trigger_Wcdma_Measurement_Prach.rst
	Trigger_Wcdma_Measurement_Tpc.rst