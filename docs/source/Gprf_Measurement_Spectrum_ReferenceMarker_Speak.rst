Speak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.ReferenceMarker.Speak.SpeakCls
	:members:
	:undoc-members:
	:noindex: