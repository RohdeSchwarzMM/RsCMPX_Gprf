Output
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:TRIGger:ADD:DEBug:OUTPut

.. code-block:: python

	DIAGnostic:TRIGger:ADD:DEBug:OUTPut



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Trigger.Add.Debug.Output.OutputCls
	:members:
	:undoc-members:
	:noindex: