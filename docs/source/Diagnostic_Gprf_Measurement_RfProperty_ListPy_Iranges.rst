Iranges
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:LIST:IRANges

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:LIST:IRANges



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.ListPy.Iranges.IrangesCls
	:members:
	:undoc-members:
	:noindex: