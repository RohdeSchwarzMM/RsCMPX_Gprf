Icomponent
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:I
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:I

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:I
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:I



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Icomponent.IcomponentCls
	:members:
	:undoc-members:
	:noindex: