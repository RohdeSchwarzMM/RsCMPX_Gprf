IqData
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData:CAPTure
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData:CAPTure
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IQData:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.IqData.IqDataCls
	:members:
	:undoc-members:
	:noindex: