BhRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:BHRate:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:BHRate:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Bluetooth.Measurement.BhRate.BhRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.bluetooth.measurement.bhRate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth_Measurement_BhRate_Catalog.rst