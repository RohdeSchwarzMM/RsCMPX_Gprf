Wpan
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wpan.WpanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wpan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Wpan_Measurement.rst