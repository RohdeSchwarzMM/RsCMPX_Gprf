Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: