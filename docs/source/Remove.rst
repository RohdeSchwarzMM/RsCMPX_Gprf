Remove
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.remove.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Remove_System.rst
	Remove_Tenvironment.rst