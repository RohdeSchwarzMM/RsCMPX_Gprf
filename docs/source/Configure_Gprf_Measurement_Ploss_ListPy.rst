ListPy
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.ploss.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Ploss_ListPy_Frequency.rst