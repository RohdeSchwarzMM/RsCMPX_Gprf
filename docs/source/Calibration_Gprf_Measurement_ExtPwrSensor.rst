ExtPwrSensor
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Calibration.Gprf.Measurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.gprf.measurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Gprf_Measurement_ExtPwrSensor_Zero.rst