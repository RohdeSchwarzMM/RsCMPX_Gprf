Enums
=========

All
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.All.ALL
	# All values (1x):
	ALL

Amplification
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Amplification.HIGH
	# All values (4x):
	HIGH | LOW | MAXimum | MEDium

ArbFile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbFile.ABSPath
	# All values (3x):
	ABSPath | DEF | TRUTh

ArbSegmentsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbSegmentsMode.AUTO
	# All values (3x):
	AUTO | CONTinuous | CSEamless

AveragingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AveragingMode.LINear
	# All values (2x):
	LINear | LOGarithmic

BasebandMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BasebandMode.ARB
	# All values (3x):
	ARB | CW | DTONe

CcdfMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CcdfMode.POWer
	# All values (2x):
	POWer | STATistic

ConnectionSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionSource.GLOBal
	# All values (2x):
	GLOBal | INDex

Detector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Detector.AUTopeak
	# All values (6x):
	AUTopeak | AVERage | MAXPeak | MINPeak | RMS | SAMPle

DetectorBasic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectorBasic.PEAK
	# All values (2x):
	PEAK | RMS

DeviceMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeviceMode.M2X2
	# All values (3x):
	M2X2 | M4X4 | NONE

DeviceType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeviceType.NONE
	# All values (2x):
	NONE | Z24

DigitalFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitalFilterType.BANDpass
	# All values (5x):
	BANDpass | CDMA | GAUSs | TDSCdma | WCDMa

ExtPwrSensorAvgMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExtPwrSensorAvgMode.MANual
	# All values (3x):
	MANual | NSR | RES

FileSave
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileSave.OFF
	# All values (3x):
	OFF | ON | ONLY

FilterCriteria
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterCriteria.LOSupport
	# All values (1x):
	LOSupport

FilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterType.B10Mhz
	# All values (5x):
	B10Mhz | B1MHz | GAUSs | NY1Mhz | NYQuist

GeneratorState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorState.ADJusted
	# All values (8x):
	ADJusted | AUTonomous | COUPled | INValid | OFF | ON | PENDing | RDY

IncTransition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IncTransition.IMMediate
	# All values (6x):
	IMMediate | RMARker | WMA1 | WMA2 | WMA3 | WMA4

InstrumentType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InstrumentType.PROTocol
	# All values (2x):
	PROTocol | SIGNaling

IqFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqFormat.IQ
	# All values (2x):
	IQ | RPHI

IqRecBypass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqRecBypass.BIT
	# All values (3x):
	BIT | OFF | ON

ListIncrement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListIncrement.ACYCles
	# All values (5x):
	ACYCles | DTIMe | MEASurement | TRIGger | USER

ListSubMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListSubMode.AUTO
	# All values (6x):
	AUTO | BBGenerator | BBMeasurement | OTHer | SINGle | STEP

LoLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoLevel.CORRect
	# All values (3x):
	CORRect | HIGH | LOW

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MagnitudeUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MagnitudeUnit.RAW
	# All values (2x):
	RAW | VOLT

MeasMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasMode.CCALibration
	# All values (3x):
	CCALibration | OOPen | OSHort

MeasScenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasScenario.CSPath
	# All values (5x):
	CSPath | MAIQ | MAPR | SALone | UNDefined

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.NORMal
	# All values (2x):
	NORMal | TALignment

NameStyle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NameStyle.FQName
	# All values (3x):
	FQName | LNAMe | NAME

OffsetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OffsetMode.FIXed
	# All values (2x):
	FIXed | VARiable

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PathLossState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathLossState.NCAP
	# All values (3x):
	NCAP | PEND | RDY

PwrSensorResolution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrSensorResolution.PD0
	# All values (4x):
	PD0 | PD1 | PD2 | PD3

Range
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Range.FULL
	# All values (2x):
	FULL | SUB

RbwFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwFilterType.BANDpass
	# All values (2x):
	BANDpass | GAUSs

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

RepeatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepeatMode.CONTinuous
	# All values (2x):
	CONTinuous | SINGle

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RfConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RfConnector.I11I
	# Last value:
	value = enums.RfConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

SelectMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelectMode.EVAL
	# All values (4x):
	EVAL | MATCh | OPEN | SHORt

SignalDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalDirection.RX
	# All values (3x):
	RX | RXTX | TX

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SignalSlopeExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlopeExt.FALLing
	# All values (4x):
	FALLing | FEDGe | REDGe | RISing

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SpanMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpanMode.FSWeep
	# All values (2x):
	FSWeep | ZSPan

Statistic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Statistic.AVERage
	# All values (4x):
	AVERage | CURRent | MAXimum | MINimum

TargetStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateA.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetStateB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateB.OFF
	# All values (3x):
	OFF | RUN | STOP

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TimeSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeSource.MANual
	# All values (2x):
	MANual | NTP

Timing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Timing.CENTered
	# All values (2x):
	CENTered | STEP

TransferMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransferMode.ENABlemode
	# All values (2x):
	ENABlemode | REQuestmode

Trigger
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Trigger.CLEarlist
	# All values (3x):
	CLEarlist | GENeratelist | OFF

TriggerPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerPowerMode.ALL
	# All values (4x):
	ALL | ONCE | PRESelect | SWEep

TriggerSequenceMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSequenceMode.ONCE
	# All values (2x):
	ONCE | PRESelect

TriggerSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSource.EXTernal
	# All values (4x):
	EXTernal | FREerun | IF | IFPower

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (86x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IFO1 | IFO2 | IFO3 | IFO4 | IFO5 | IFO6 | IQ2O | IQ4O
	IQ6O | IQ8O | R10D | R118 | R1183 | R1184 | R11C | R11D
	R11O | R11O3 | R11O4 | R12C | R12D | R13C | R13O | R14C
	R214 | R218 | R21C | R21O | R22C | R23C | R23O | R24C
	R258 | R318 | R31C | R31O | R32C | R33C | R33O | R34C
	R418 | R41C | R41O | R42C | R43C | R43O | R44C | RA18
	RB14 | RB18 | RC18 | RD18 | RE18 | RF18 | RF1C | RF1O
	RF2C | RF3C | RF3O | RF4C | RF5C | RF6C | RF7C | RF8C
	RF9C | RFAC | RFAO | RFBC | RG18 | RH18

TxiMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxiMode.IREPetition
	# All values (2x):
	IREPetition | LENTry

UserDebugMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UserDebugMode.DEBug
	# All values (2x):
	DEBug | USER

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

ZeroingState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ZeroingState.FAILed
	# All values (2x):
	FAILed | PASSed

