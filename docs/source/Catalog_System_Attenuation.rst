Attenuation
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.System.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.system.attenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_System_Attenuation_CorrectionTable.rst