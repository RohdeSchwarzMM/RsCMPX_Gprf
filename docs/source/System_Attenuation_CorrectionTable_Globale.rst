Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	DELete:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Gprf.Implementations.System.Attenuation.CorrectionTable.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: