Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Average.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: