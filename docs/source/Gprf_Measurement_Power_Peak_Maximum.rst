Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	single: FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	single: READ:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum
	READ:GPRF:MEASurement<Instance>:POWer:PEAK:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: