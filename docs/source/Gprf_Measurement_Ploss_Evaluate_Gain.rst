Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:GAIN

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:GAIN



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: