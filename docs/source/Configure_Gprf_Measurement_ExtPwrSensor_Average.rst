Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:COUNt
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:APERture

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:MODE
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:COUNt
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AVERage:APERture



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.ExtPwrSensor.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: