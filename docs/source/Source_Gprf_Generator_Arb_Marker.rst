Marker
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Arb.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.arb.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Arb_Marker_Delays.rst