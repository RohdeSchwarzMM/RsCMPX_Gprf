Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:SSTop

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:SSTop



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqVsSlot.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: