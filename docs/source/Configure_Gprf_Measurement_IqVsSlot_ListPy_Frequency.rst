Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:FREQuency:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:FREQuency:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqVsSlot.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: