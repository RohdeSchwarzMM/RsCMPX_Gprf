Delays
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:MARKer:DELays

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:MARKer:DELays



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Arb.Marker.Delays.DelaysCls
	:members:
	:undoc-members:
	:noindex: