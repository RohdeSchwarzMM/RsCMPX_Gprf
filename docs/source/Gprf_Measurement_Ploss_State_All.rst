All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:STATe:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: