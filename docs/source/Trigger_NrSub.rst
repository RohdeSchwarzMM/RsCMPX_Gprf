NrSub
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.NrSubCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSub.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSub_Measurement.rst