Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:GAUSs:BWIDth

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:IQRecorder:GAUSs:BWIDth



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.IqRecorder.Gauss.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: