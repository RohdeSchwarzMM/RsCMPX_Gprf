Idx<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix32
	rc = driver.configure.gprf.measurement.power.listPy.idx.repcap_index_get()
	driver.configure.gprf.measurement.power.listPy.idx.repcap_index_set(repcap.Index.Ix1)





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Idx.IdxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.power.listPy.idx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Power_ListPy_Idx_Catalog.rst
	Configure_Gprf_Measurement_Power_ListPy_Idx_Connection.rst