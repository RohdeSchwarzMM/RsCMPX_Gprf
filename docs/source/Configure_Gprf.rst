Gprf
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.GprfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Generator.rst
	Configure_Gprf_Measurement.rst