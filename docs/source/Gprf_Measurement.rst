Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Canalyzer.rst
	Gprf_Measurement_ExtPwrSensor.rst
	Gprf_Measurement_FftSpecAn.rst
	Gprf_Measurement_IqRecorder.rst
	Gprf_Measurement_IqVsSlot.rst
	Gprf_Measurement_Nrpm.rst
	Gprf_Measurement_Ploss.rst
	Gprf_Measurement_Power.rst
	Gprf_Measurement_Spectrum.rst