Apool
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:VALid
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:LOADed
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RREQuired
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RTOTal
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:FILE
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:REMove
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CLEar
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:MINDex

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:VALid
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:LOADed
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RREQuired
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RTOTal
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:FILE
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:REMove
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CLEar
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:MINDex



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.ApoolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.apool.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_Apool_Check.rst
	Source_Gprf_Generator_Sequencer_Apool_CrcProtect.rst
	Source_Gprf_Generator_Sequencer_Apool_Download.rst
	Source_Gprf_Generator_Sequencer_Apool_Duration.rst
	Source_Gprf_Generator_Sequencer_Apool_Paratio.rst
	Source_Gprf_Generator_Sequencer_Apool_Path.rst
	Source_Gprf_Generator_Sequencer_Apool_Poffset.rst
	Source_Gprf_Generator_Sequencer_Apool_Reliability.rst
	Source_Gprf_Generator_Sequencer_Apool_Rmessage.rst
	Source_Gprf_Generator_Sequencer_Apool_Roption.rst
	Source_Gprf_Generator_Sequencer_Apool_Samples.rst
	Source_Gprf_Generator_Sequencer_Apool_SymbolRate.rst
	Source_Gprf_Generator_Sequencer_Apool_Waveform.rst