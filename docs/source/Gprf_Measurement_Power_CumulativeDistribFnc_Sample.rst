Sample
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:SAMPle

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:SAMPle



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.CumulativeDistribFnc.Sample.SampleCls
	:members:
	:undoc-members:
	:noindex: