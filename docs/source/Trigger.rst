Trigger
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth.rst
	Trigger_Cdma.rst
	Trigger_Gprf.rst
	Trigger_Gsm.rst
	Trigger_Lte.rst
	Trigger_LteDl.rst
	Trigger_Niot.rst
	Trigger_NrDl.rst
	Trigger_NrMmw.rst
	Trigger_NrSub.rst
	Trigger_Uwb.rst
	Trigger_Wcdma.rst
	Trigger_Wlan.rst
	Trigger_Wpan.rst