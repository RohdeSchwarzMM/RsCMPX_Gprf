Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.NrMmw.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.nrMmw.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_NrMmw_Measurement_Spath.rst