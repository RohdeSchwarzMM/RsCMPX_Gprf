All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:STATe:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: