Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTe:GPRF:MEASurement<Instance>:SPATh

.. code-block:: python

	DIAGnostic:ROUTe:GPRF:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Route.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: