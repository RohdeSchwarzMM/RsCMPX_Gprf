Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTe:UWB:MEASurement<Instance>:SPATh

.. code-block:: python

	DIAGnostic:ROUTe:UWB:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Route.Uwb.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: