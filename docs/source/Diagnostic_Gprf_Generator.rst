Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:PNMode

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:PNMode



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.gprf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Gprf_Generator_Correction.rst
	Diagnostic_Gprf_Generator_RfProperty.rst
	Diagnostic_Gprf_Generator_RfSettings.rst
	Diagnostic_Gprf_Generator_RfSetttings.rst
	Diagnostic_Gprf_Generator_Rms.rst
	Diagnostic_Gprf_Generator_Snumber.rst