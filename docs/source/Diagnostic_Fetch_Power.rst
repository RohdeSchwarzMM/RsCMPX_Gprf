Power
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Fetch.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.fetch.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Fetch_Power_State.rst