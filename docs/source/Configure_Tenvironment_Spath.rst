Spath
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tenvironment.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tenvironment_Spath_Attenuation.rst
	Configure_Tenvironment_Spath_CorrectionTable.rst
	Configure_Tenvironment_Spath_Direction.rst
	Configure_Tenvironment_Spath_Info.rst