Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.NrDl.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.nrDl.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_NrDl_Measurement_Spath.rst