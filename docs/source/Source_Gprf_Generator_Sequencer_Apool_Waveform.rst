Waveform
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:WAVeform:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:WAVeform

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:WAVeform:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:WAVeform



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.Waveform.WaveformCls
	:members:
	:undoc-members:
	:noindex: