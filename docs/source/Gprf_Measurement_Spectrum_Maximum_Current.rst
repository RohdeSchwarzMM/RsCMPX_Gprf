Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Maximum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: