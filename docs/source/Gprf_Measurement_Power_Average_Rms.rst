Rms
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:AVERage:RMS
	single: READ:GPRF:MEASurement<Instance>:POWer:AVERage:RMS

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:AVERage:RMS
	READ:GPRF:MEASurement<Instance>:POWer:AVERage:RMS



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Average.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: