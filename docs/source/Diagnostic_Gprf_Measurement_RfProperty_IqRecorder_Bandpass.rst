Bandpass
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.IqRecorder.Bandpass.BandpassCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.gprf.measurement.rfProperty.iqRecorder.bandpass.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Gprf_Measurement_RfProperty_IqRecorder_Bandpass_Bandwidth.rst