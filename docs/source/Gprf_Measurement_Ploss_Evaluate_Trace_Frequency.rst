Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:TRACe:FREQuency

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:TRACe:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.Trace.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: