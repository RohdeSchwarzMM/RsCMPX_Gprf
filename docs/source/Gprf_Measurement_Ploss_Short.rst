Short
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<instance>:PLOSs:SHORt

.. code-block:: python

	INITiate:GPRF:MEASurement<instance>:PLOSs:SHORt



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Short.ShortCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.short.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Short_State.rst