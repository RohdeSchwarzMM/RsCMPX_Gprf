Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: