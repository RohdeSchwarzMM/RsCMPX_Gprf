Psub
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:PAYLoad
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:PAYLoad
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.File.Psub.PsubCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.dapi.logging.file.psub.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Dapi_Logging_File_Psub_FilterPy.rst