Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.FftSpecAn.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: