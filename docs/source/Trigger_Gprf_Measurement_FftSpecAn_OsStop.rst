OsStop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OSSTop

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OSSTop



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.FftSpecAn.OsStop.OsStopCls
	:members:
	:undoc-members:
	:noindex: