Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Sample.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: