Rmarker
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:RMARker:DELay

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:RMARker:DELay



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Rmarker.RmarkerCls
	:members:
	:undoc-members:
	:noindex: