All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.ExtPwrSensor.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: