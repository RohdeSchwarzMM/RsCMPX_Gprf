System
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SYSTem:POSitioner

.. code-block:: python

	CATalog:SYSTem:POSitioner



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.System.SystemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_System_Attenuation.rst
	Catalog_System_Reset.rst
	Catalog_System_Rf42.rst
	Catalog_System_Rrhead.rst