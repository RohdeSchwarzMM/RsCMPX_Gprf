Download
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DOWNload

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DOWNload



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.Download.DownloadCls
	:members:
	:undoc-members:
	:noindex: