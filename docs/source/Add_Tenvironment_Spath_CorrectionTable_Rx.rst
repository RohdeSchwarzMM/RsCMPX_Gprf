Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:TENVironment:SPATh:CTABle:RX

.. code-block:: python

	ADD:TENVironment:SPATh:CTABle:RX



.. autoclass:: RsCMPX_Gprf.Implementations.Add.Tenvironment.Spath.CorrectionTable.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: