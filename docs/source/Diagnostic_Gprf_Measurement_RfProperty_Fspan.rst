Fspan
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FSPan

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FSPan



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.Fspan.FspanCls
	:members:
	:undoc-members:
	:noindex: