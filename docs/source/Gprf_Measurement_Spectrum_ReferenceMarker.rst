ReferenceMarker
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_ReferenceMarker_Npeak.rst
	Gprf_Measurement_Spectrum_ReferenceMarker_Speak.rst