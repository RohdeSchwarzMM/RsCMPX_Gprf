Dapi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GENeric:MEASurement:DAPI:TOUT

.. code-block:: python

	DIAGnostic:GENeric:MEASurement:DAPI:TOUT



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Generic.Measurement.Dapi.DapiCls
	:members:
	:undoc-members:
	:noindex: