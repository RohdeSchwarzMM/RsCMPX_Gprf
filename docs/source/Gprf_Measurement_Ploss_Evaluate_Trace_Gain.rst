Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:TRACe:GAIN

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:TRACe:GAIN



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.Trace.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: