Rms
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:RMS
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent:RMS

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:RMS
	READ:GPRF:MEASurement<Instance>:POWer:CURRent:RMS



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Current.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: