Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:CONFigure:GPRF:MEASurement:LOGGing

.. code-block:: python

	DIAGnostic:CONFigure:GPRF:MEASurement:LOGGing



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: