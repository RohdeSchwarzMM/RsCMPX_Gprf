Rnames
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:RNAMes

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:RNAMes



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.Ploss.Rnames.RnamesCls
	:members:
	:undoc-members:
	:noindex: