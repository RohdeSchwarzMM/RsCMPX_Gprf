Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSub.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSub_Measurement_MultiEval.rst
	Trigger_NrSub_Measurement_Prach.rst
	Trigger_NrSub_Measurement_Srs.rst