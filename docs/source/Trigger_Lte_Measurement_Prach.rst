Prach
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:SOURce

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:PRACh:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Lte.Measurement.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.lte.measurement.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Lte_Measurement_Prach_Catalog.rst