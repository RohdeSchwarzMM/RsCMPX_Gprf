Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Cdma.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.cdma.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Cdma_Measurement_MultiEval.rst