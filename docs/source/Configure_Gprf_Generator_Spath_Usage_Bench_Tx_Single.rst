Single
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe:BENCh<nr>:TX:SINGle

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe:BENCh<nr>:TX:SINGle



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Generator.Spath.Usage.Bench.Tx.Single.SingleCls
	:members:
	:undoc-members:
	:noindex: