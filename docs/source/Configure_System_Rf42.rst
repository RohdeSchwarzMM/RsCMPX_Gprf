Rf42
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rf42.Rf42Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.rf42.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Rf42_Box.rst