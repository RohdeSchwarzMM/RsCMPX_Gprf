PeakSearch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:PSEarch:NOAMarkers
	single: CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:PSEarch

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:PSEarch:NOAMarkers
	CONFigure:GPRF:MEASurement<Instance>:FFTSanalyzer:PSEarch



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.FftSpecAn.PeakSearch.PeakSearchCls
	:members:
	:undoc-members:
	:noindex: