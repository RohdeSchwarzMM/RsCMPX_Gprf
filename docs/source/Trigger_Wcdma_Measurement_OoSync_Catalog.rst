Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:OOSYnc:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.Measurement.OoSync.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: