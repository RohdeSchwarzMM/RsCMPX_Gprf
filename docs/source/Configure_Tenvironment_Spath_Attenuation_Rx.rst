Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:ATTenuation:RX

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:ATTenuation:RX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.Attenuation.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: