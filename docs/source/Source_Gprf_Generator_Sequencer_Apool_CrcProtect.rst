CrcProtect
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CRCProtect:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CRCProtect

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CRCProtect:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CRCProtect



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.CrcProtect.CrcProtectCls
	:members:
	:undoc-members:
	:noindex: