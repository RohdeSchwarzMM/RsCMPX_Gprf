Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:CATalog:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:SRS:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.Measurement.Srs.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: