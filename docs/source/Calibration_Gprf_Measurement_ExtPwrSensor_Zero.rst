Zero
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:GPRF:MEASurement<Instance>:EPSensor:ZERO

.. code-block:: python

	CALibration:GPRF:MEASurement<Instance>:EPSensor:ZERO



.. autoclass:: RsCMPX_Gprf.Implementations.Calibration.Gprf.Measurement.ExtPwrSensor.Zero.ZeroCls
	:members:
	:undoc-members:
	:noindex: