Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Spectrum_FreqSweep.rst
	Configure_Gprf_Measurement_Spectrum_Frequency.rst
	Configure_Gprf_Measurement_Spectrum_ZeroSpan.rst