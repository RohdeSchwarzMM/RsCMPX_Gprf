To
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:POSitioner<PositionerIdx>:MOVE:TO

.. code-block:: python

	[CONFigure]:SYSTem:POSitioner<PositionerIdx>:MOVE:TO



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.Move.To.ToCls
	:members:
	:undoc-members:
	:noindex: