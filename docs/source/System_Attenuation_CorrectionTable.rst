CorrectionTable
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.System.Attenuation.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.attenuation.correctionTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Attenuation_CorrectionTable_All.rst
	System_Attenuation_CorrectionTable_Globale.rst
	System_Attenuation_CorrectionTable_Tenvironment.rst