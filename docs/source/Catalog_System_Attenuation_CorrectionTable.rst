CorrectionTable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CATalog:SYSTem:ATTenuation:CTABle[:TENVironment]
	single: CATalog:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	CATalog:SYSTem:ATTenuation:CTABle[:TENVironment]
	CATalog:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.System.Attenuation.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex: