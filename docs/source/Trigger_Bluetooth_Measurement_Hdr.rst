Hdr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BLUetooth:MEASurement<Instance>:HDR:SOURce

.. code-block:: python

	TRIGger:BLUetooth:MEASurement<Instance>:HDR:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Bluetooth.Measurement.Hdr.HdrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.bluetooth.measurement.hdr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth_Measurement_Hdr_Catalog.rst