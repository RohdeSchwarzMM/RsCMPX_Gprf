Nrpm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRPM:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:NRPM:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:NRPM:TOUT

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRPM:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:NRPM:REPetition
	CONFigure:GPRF:MEASurement<Instance>:NRPM:TOUT



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Nrpm.NrpmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.nrpm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Nrpm_Sensor.rst