MultiEval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:CDMA:MEASurement<Instance>:MEValuation:SOURce

.. code-block:: python

	TRIGger:CDMA:MEASurement<Instance>:MEValuation:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Cdma.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.cdma.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Cdma_Measurement_MultiEval_Catalog.rst