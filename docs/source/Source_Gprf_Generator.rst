Generator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:BBMode

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:BBMode



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.GeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Arb.rst
	Source_Gprf_Generator_Dtone.rst
	Source_Gprf_Generator_IqSettings.rst
	Source_Gprf_Generator_ListPy.rst
	Source_Gprf_Generator_Reliability.rst
	Source_Gprf_Generator_RfSettings.rst
	Source_Gprf_Generator_Sequencer.rst
	Source_Gprf_Generator_State.rst