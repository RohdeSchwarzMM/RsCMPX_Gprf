Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:MAXimum
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:MAXimum
	READ:GPRF:MEASurement<Instance>:POWer:CURRent:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Current.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: