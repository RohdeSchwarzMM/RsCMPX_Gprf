IqData
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:IQData
	single: READ:GPRF:MEASurement<Instance>:POWer:IQData

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:IQData
	READ:GPRF:MEASurement<Instance>:POWer:IQData



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.IqData.IqDataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.iqData.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_IqData_Bin.rst