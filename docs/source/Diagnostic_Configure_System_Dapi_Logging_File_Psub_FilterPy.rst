FilterPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:FILTer:MNAMe
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:FILTer:RNAMe

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:FILTer:MNAMe
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:PSUB:FILTer:RNAMe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.File.Psub.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: