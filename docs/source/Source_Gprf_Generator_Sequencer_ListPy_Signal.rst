Signal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.listPy.signal.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_ListPy_Signal_All.rst
	Source_Gprf_Generator_Sequencer_ListPy_Signal_Catalog.rst
	Source_Gprf_Generator_Sequencer_ListPy_Signal_Index.rst
	Source_Gprf_Generator_Sequencer_ListPy_Signal_Range.rst