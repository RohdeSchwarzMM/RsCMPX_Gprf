Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SSTop

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SSTop



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: