Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:RMS:OFFSet

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:RMS:OFFSet



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: