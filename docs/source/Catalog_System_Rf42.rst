Rf42
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.System.Rf42.Rf42Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.system.rf42.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_System_Rf42_Box.rst