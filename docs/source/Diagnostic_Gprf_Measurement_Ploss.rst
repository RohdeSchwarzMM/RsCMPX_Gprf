Ploss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:CCALibration
	single: DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:SMODe

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:CCALibration
	DIAGnostic:GPRF:MEASurement<Instance>:PLOSs:SMODe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.gprf.measurement.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Gprf_Measurement_Ploss_Rnames.rst