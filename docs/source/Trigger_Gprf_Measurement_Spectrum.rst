Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SLOPe
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:TOUT

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:THReshold
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SLOPe
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:MGAP
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:TOUT



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: