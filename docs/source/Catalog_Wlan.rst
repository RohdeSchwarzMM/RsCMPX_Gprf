Wlan
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Wlan.WlanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.wlan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Wlan_Measurement.rst