LteDl
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.LteDl.LteDlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.lteDl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_LteDl_Measurement.rst