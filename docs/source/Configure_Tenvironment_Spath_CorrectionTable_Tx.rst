Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:CTABle:TX

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:CTABle:TX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.CorrectionTable.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: