Bandwidth
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:GAUSs:BWIDth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:GAUSs:BWIDth:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:GAUSs:BWIDth
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:GAUSs:BWIDth:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.FilterPy.Gauss.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: