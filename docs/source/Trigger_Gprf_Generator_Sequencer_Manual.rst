Manual
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Sequencer.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.sequencer.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Sequencer_Manual_Execute.rst