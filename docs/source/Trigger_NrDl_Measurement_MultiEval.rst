MultiEval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRDL:MEASurement<Instance>:MEValuation:SOURce

.. code-block:: python

	TRIGger:NRDL:MEASurement<Instance>:MEValuation:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrDl.Measurement.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrDl.measurement.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrDl_Measurement_MultiEval_Catalog.rst