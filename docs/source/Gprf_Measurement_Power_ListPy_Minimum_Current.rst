Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:LIST:MINimum:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Minimum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: