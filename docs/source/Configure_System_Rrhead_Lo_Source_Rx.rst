Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SYSTem:RRHead:LO:SOURce:RX

.. code-block:: python

	CONFigure:SYSTem:RRHead:LO:SOURce:RX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rrhead.Lo.Source.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: