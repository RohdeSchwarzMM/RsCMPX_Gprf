Globale
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SYSTem:ATTenuation:CTABle:GLOBal

.. code-block:: python

	ADD:SYSTem:ATTenuation:CTABle:GLOBal



.. autoclass:: RsCMPX_Gprf.Implementations.Add.System.Attenuation.CorrectionTable.Globale.GlobaleCls
	:members:
	:undoc-members:
	:noindex: