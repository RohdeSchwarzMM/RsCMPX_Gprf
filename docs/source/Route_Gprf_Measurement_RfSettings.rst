RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Measurement.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: