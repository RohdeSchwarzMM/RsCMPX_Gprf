View
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:VIEW:AFTaps

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:VIEW:AFTaps



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.View.ViewCls
	:members:
	:undoc-members:
	:noindex: