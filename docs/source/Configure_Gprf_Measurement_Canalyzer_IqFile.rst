IqFile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:IQFile

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:IQFile



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Canalyzer.IqFile.IqFileCls
	:members:
	:undoc-members:
	:noindex: