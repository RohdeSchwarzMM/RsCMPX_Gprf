Phase
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:PHASe
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:PHASe

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:PHASe
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:PHASe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: