Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MAXimum:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Maximum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: