Rbw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:TYPE
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:BANDpass
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:GAUSs

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:TYPE
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:BANDpass
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:RBW:GAUSs



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.ZeroSpan.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex: