Temperature
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Sense.Base.Temperature.TemperatureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.base.temperature.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base_Temperature_Operating.rst