FftSpecAn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_FftSpecAn_Icomponent.rst
	Gprf_Measurement_FftSpecAn_Peaks.rst
	Gprf_Measurement_FftSpecAn_Power.rst
	Gprf_Measurement_FftSpecAn_Qcomponent.rst
	Gprf_Measurement_FftSpecAn_State.rst