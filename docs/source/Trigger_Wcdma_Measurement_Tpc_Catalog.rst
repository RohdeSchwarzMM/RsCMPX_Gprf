Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:TPC:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.Measurement.Tpc.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: