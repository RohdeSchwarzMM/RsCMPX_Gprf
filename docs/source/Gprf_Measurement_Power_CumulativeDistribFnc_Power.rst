Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:POWer

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:POWer



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.CumulativeDistribFnc.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: