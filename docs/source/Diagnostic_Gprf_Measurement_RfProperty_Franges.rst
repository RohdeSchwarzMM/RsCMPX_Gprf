Franges
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FRANges:MINDex

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FRANges:MINDex



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.Franges.FrangesCls
	:members:
	:undoc-members:
	:noindex: