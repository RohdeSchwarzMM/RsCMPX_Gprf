Measurement
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RLEVel
	single: DIAGnostic:GPRF:MEASurement<Instance>:DEBug
	single: DIAGnostic:GPRF:MEASurement:VERSion

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RLEVel
	DIAGnostic:GPRF:MEASurement<Instance>:DEBug
	DIAGnostic:GPRF:MEASurement:VERSion



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.gprf.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Gprf_Measurement_Ploss.rst
	Diagnostic_Gprf_Measurement_RfProperty.rst
	Diagnostic_Gprf_Measurement_Snumber.rst