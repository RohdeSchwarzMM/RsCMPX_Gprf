Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Bluetooth.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.bluetooth.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Bluetooth_Measurement_BhRate.rst
	Trigger_Bluetooth_Measurement_Hdr.rst
	Trigger_Bluetooth_Measurement_Hdrp.rst
	Trigger_Bluetooth_Measurement_MultiEval.rst