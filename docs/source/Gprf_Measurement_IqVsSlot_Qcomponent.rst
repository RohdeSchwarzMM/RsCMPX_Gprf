Qcomponent
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:Q
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:Q

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:Q
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:Q



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.Qcomponent.QcomponentCls
	:members:
	:undoc-members:
	:noindex: