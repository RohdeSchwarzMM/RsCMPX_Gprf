Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Rms.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: