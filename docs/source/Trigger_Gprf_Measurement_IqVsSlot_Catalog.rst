Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.IqVsSlot.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: