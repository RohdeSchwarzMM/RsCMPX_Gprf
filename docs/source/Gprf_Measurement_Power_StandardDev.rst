StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: READ:GPRF:MEASurement<Instance>:POWer:SDEViation

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	READ:GPRF:MEASurement<Instance>:POWer:SDEViation



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.standardDev.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_StandardDev_Current.rst