ZeroSpan
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:SWT
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:DEBug

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:SWT
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:DEBug



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.ZeroSpan.ZeroSpanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.spectrum.zeroSpan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Spectrum_ZeroSpan_Rbw.rst
	Configure_Gprf_Measurement_Spectrum_ZeroSpan_Vbw.rst