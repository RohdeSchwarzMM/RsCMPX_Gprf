Gprf
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.Gprf.GprfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.gprf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_Gprf_Measurement.rst