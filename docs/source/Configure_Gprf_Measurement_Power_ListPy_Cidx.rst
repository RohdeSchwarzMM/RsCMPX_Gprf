Cidx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:CIDX

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:CIDX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Cidx.CidxCls
	:members:
	:undoc-members:
	:noindex: