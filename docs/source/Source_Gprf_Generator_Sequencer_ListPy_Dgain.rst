Dgain
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DGAin
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DGAin:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DGAin
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:DGAin:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Dgain.DgainCls
	:members:
	:undoc-members:
	:noindex: