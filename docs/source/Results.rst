Results
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Results.ResultsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.results.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Results_Gprf.rst