Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:FREQuency

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:EVALuate:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: