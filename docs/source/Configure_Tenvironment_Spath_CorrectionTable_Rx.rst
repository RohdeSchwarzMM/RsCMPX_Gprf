Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:CTABle:RX

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:CTABle:RX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.CorrectionTable.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: