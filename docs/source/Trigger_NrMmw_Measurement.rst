Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrMmw.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrMmw.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrMmw_Measurement_MultiEval.rst
	Trigger_NrMmw_Measurement_Prach.rst