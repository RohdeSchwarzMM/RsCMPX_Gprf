Spath<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.catalog.niot.measurement.spath.repcap_stream_get()
	driver.catalog.niot.measurement.spath.repcap_stream_set(repcap.Stream.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CATalog:NIOT:MEASurement<Instance>:SPATh<StreamNumber>

.. code-block:: python

	CATalog:NIOT:MEASurement<Instance>:SPATh<StreamNumber>



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Niot.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.niot.measurement.spath.clone()