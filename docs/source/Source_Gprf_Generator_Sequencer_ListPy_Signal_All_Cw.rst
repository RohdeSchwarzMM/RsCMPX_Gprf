Cw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:CW

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL:CW



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Signal.All.Cw.CwCls
	:members:
	:undoc-members:
	:noindex: