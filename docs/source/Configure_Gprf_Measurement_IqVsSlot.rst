IqVsSlot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:MLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:SLENgth
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:FTYPe
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:FELimit

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:TOUT
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:REPetition
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:MLENgth
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:SLENgth
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:FTYPe
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:FELimit



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqVsSlot.IqVsSlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.iqVsSlot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_IqVsSlot_ListPy.rst
	Configure_Gprf_Measurement_IqVsSlot_Trigger.rst