Bandwidth
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BWIDth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BWIDth:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BWIDth
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BWIDth:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.FilterPy.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: