Uwb
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Uwb.UwbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.uwb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Uwb_Measurement.rst