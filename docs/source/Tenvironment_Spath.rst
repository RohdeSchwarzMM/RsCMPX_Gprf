Spath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DELete:TENVironment:SPATh

.. code-block:: python

	DELete:TENVironment:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Tenvironment.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: