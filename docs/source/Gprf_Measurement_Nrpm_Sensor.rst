Sensor<Sensor>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.gprf.measurement.nrpm.sensor.repcap_sensor_get()
	driver.gprf.measurement.nrpm.sensor.repcap_sensor_set(repcap.Sensor.Nr1)





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Nrpm.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.nrpm.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Nrpm_Sensor_Power.rst