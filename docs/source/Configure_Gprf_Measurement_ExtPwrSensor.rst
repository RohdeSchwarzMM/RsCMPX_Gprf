ExtPwrSensor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:RESolution
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:SCOunt
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:TOUT
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:RESolution
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:SCOunt
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:REPetition
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_ExtPwrSensor_Attenuation.rst
	Configure_Gprf_Measurement_ExtPwrSensor_Auto.rst
	Configure_Gprf_Measurement_ExtPwrSensor_Average.rst