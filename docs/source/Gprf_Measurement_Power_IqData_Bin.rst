Bin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:IQData:BIN

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:IQData:BIN



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.IqData.Bin.BinCls
	:members:
	:undoc-members:
	:noindex: