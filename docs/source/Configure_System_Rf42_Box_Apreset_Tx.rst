Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:RF42:BOX<BoxNo>:APReset:TX

.. code-block:: python

	[CONFigure]:SYSTem:RF42:BOX<BoxNo>:APReset:TX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rf42.Box.Apreset.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: