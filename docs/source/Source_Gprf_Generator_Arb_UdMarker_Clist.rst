Clist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:UDMarker:CLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:UDMarker:CLISt



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Arb.UdMarker.Clist.ClistCls
	:members:
	:undoc-members:
	:noindex: