FilterPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:FILTer:MNAMe
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:FILTer:RNAMe

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:FILTer:MNAMe
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:FILE:RPC:FILTer:RNAMe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.File.Rpc.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: