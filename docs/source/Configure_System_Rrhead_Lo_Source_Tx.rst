Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SYSTem:RRHead:LO:SOURce:TX

.. code-block:: python

	CONFigure:SYSTem:RRHead:LO:SOURce:TX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rrhead.Lo.Source.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: