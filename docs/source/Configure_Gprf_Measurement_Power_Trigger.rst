Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<Instance>:POWer:TRIGger:SOURce

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<Instance>:POWer:TRIGger:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: