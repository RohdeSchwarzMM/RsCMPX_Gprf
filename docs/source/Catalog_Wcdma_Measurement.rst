Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Wcdma.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.wcdma.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Wcdma_Measurement_Spath.rst