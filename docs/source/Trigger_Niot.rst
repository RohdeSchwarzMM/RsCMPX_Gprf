Niot
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Niot.NiotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.niot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Niot_Measurement.rst