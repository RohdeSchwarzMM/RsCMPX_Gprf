Vbw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:VBW



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Spectrum.ZeroSpan.Vbw.VbwCls
	:members:
	:undoc-members:
	:noindex: