Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NIOT:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:NIOT:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Niot.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: