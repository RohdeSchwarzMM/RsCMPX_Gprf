Probability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:PROBability

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:PROBability



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.CumulativeDistribFnc.Probability.ProbabilityCls
	:members:
	:undoc-members:
	:noindex: