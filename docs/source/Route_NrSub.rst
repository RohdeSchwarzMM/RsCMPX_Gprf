NrSub
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrSub.NrSubCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrSub.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrSub_Measurement.rst