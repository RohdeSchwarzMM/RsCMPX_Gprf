Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTEDl:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:LTEDl:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:LTEDl:MEASurement<Instance>:SPATh:COUNt
	ROUTe:LTEDl:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.LteDl.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: