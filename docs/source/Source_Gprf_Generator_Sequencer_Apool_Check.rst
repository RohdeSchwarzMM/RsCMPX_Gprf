Check
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CHECk

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:CHECk



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.Check.CheckCls
	:members:
	:undoc-members:
	:noindex: