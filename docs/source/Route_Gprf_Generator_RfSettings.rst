RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:GENerator<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:GPRF:GENerator<Instance>:RFSettings:CONNector



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: