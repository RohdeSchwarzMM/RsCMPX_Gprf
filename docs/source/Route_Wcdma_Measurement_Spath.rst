Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:WCDMa:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:WCDMa:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:WCDMa:MEASurement<Instance>:SPATh:COUNt
	ROUTe:WCDMa:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wcdma.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: