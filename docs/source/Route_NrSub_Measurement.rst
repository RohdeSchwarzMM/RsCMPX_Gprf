Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrSub.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrSub.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrSub_Measurement_Spath.rst