Cdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Cdma.CdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.cdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Cdma_Measurement.rst