Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.IqRecorder.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: