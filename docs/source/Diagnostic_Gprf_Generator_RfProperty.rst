RfProperty
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:RFPRoperty:FRANges

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:RFPRoperty:FRANges



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.RfProperty.RfPropertyCls
	:members:
	:undoc-members:
	:noindex: