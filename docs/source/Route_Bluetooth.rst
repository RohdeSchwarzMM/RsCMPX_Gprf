Bluetooth
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Bluetooth.BluetoothCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.bluetooth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Bluetooth_Measurement.rst