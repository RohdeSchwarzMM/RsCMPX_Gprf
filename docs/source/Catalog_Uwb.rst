Uwb
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Uwb.UwbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.uwb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Uwb_Measurement.rst