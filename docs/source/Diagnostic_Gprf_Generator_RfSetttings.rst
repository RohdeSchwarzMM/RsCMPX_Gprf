RfSetttings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:RFSetttings:PARatio

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:RFSetttings:PARatio



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.RfSetttings.RfSetttingsCls
	:members:
	:undoc-members:
	:noindex: