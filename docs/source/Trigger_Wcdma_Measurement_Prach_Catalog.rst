Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:PRACh:CATalog:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:PRACh:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.Measurement.Prach.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: