Group
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CATalog:GPRF:GENerator<Instance>:SPATh:GROup:CONNector
	single: CATalog:GPRF:GENerator<Instance>:SPATh:GROup

.. code-block:: python

	CATalog:GPRF:GENerator<Instance>:SPATh:GROup:CONNector
	CATalog:GPRF:GENerator<Instance>:SPATh:GROup



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Gprf.Generator.Spath.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: