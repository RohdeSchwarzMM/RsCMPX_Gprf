Offset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:PSET:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:PSET:OFFSet:ALL

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:POWer:PSET:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:POWer:PSET:OFFSet:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.Power.ParameterSetList.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: