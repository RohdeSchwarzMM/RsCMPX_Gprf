ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:TXIMode
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:TXITiming
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FILL
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:MUNit
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:COUNt
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:STARt
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:STOP
	single: [CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:CSOurce
	single: [CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:NIDX
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:TXIMode
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:TXITiming
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:FILL
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:MUNit
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:COUNt
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:STARt
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:STOP
	[CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:CSOurce
	[CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:NIDX
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.power.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Power_ListPy_Cidx.rst
	Configure_Gprf_Measurement_Power_ListPy_EnvelopePower.rst
	Configure_Gprf_Measurement_Power_ListPy_Frequency.rst
	Configure_Gprf_Measurement_Power_ListPy_Idx.rst
	Configure_Gprf_Measurement_Power_ListPy_IqData.rst
	Configure_Gprf_Measurement_Power_ListPy_Irepetition.rst
	Configure_Gprf_Measurement_Power_ListPy_ParameterSetList.rst
	Configure_Gprf_Measurement_Power_ListPy_Retrigger.rst
	Configure_Gprf_Measurement_Power_ListPy_Sstop.rst