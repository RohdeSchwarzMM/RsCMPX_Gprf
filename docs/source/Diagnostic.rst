Diagnostic
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.DiagnosticCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Catalog.rst
	Diagnostic_Configure.rst
	Diagnostic_Fetch.rst
	Diagnostic_Generic.rst
	Diagnostic_Gprf.rst
	Diagnostic_Meas.rst
	Diagnostic_Route.rst
	Diagnostic_Trigger.rst