Save
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Save.SaveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.save.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Save_Partial.rst