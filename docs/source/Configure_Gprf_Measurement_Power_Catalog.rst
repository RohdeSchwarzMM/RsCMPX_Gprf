Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:CATalog:PDEFset

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:CATalog:PDEFset



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: