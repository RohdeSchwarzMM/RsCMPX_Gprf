Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum
	READ:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: