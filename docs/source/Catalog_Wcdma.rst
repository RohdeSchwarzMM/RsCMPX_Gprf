Wcdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Wcdma_Measurement.rst