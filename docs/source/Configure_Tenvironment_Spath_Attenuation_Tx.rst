Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:ATTenuation:TX

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:ATTenuation:TX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.Attenuation.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: