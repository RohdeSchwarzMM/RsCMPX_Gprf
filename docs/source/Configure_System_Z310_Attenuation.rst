Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:Z310:ATTenuation

.. code-block:: python

	[CONFigure]:SYSTem:Z310:ATTenuation



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Z310.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: