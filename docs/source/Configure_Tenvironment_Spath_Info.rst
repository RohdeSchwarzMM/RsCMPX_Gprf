Info
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:INFO

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:INFO



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.Info.InfoCls
	:members:
	:undoc-members:
	:noindex: