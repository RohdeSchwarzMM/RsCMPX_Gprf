Slist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:SLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:SLISt



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Slist.SlistCls
	:members:
	:undoc-members:
	:noindex: