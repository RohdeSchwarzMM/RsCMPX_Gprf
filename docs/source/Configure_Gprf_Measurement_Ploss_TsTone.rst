TsTone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:ENABle

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TSTone:ENABle



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.TsTone.TsToneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.ploss.tsTone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Ploss_TsTone_File.rst