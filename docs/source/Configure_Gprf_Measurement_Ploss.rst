Ploss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TRACe
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:MMODe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TRACe
	CONFigure:GPRF:MEASurement<Instance>:PLOSs:MMODe



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.measurement.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Measurement_Ploss_ListPy.rst
	Configure_Gprf_Measurement_Ploss_TsTone.rst
	Configure_Gprf_Measurement_Ploss_View.rst