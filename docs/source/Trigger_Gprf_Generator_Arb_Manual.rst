Manual
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.arb.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Arb_Manual_Execute.rst