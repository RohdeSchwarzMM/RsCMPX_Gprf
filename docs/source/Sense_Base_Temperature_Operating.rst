Operating
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Sense.Base.Temperature.Operating.OperatingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.base.temperature.operating.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Base_Temperature_Operating_Ambient.rst