Sequencer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:TOUT

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:TOUT



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.sequencer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Sequencer_IsMeas.rst
	Trigger_Gprf_Generator_Sequencer_IsTrigger.rst
	Trigger_Gprf_Generator_Sequencer_Manual.rst