Bluetooth
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Bluetooth.BluetoothCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.bluetooth.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Bluetooth_Measurement.rst