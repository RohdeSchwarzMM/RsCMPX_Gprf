Rms
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Rms_Average.rst
	Gprf_Measurement_Spectrum_Rms_Current.rst
	Gprf_Measurement_Spectrum_Rms_Maximum.rst
	Gprf_Measurement_Spectrum_Rms_Minimum.rst