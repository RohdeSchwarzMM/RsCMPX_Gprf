Npeak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:NPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:NPEak



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.ReferenceMarker.Npeak.NpeakCls
	:members:
	:undoc-members:
	:noindex: