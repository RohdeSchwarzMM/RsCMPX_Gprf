Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Minimum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: