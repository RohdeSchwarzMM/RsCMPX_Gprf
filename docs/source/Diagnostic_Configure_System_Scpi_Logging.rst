Logging
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:SCPI:LOGGing:FILE
	single: DIAGnostic[:CONFigure]:SYSTem:SCPI:LOGGing:MARS

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:SCPI:LOGGing:FILE
	DIAGnostic[:CONFigure]:SYSTem:SCPI:LOGGing:MARS



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Scpi.Logging.LoggingCls
	:members:
	:undoc-members:
	:noindex: