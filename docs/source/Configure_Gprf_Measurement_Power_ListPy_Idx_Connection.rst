Connection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:IDX<idx>:CONNection

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:IDX<idx>:CONNection



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Idx.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex: