Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:FREQuency

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EEPRom:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Eeprom.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: