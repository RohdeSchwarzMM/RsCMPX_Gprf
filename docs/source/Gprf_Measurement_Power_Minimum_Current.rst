Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:MINimum:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Minimum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: