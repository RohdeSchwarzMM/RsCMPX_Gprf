Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrDl.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrDl.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrDl_Measurement_MultiEval.rst