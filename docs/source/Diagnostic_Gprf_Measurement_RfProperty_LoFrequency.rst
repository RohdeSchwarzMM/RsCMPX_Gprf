LoFrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:LOFRequency:AVAilable

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:LOFRequency:AVAilable



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.LoFrequency.LoFrequencyCls
	:members:
	:undoc-members:
	:noindex: