Icomponent
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:I
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:I

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:I
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:I



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.Icomponent.IcomponentCls
	:members:
	:undoc-members:
	:noindex: