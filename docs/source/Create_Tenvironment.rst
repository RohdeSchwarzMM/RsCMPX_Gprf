Tenvironment
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Create.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.tenvironment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_Tenvironment_Spath.rst