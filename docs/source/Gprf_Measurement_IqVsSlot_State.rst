State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQVSlot:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.iqVsSlot.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_IqVsSlot_State_All.rst