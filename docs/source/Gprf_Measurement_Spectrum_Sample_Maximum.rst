Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Sample.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: