Attenuation
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Modify.System.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.modify.system.attenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Modify_System_Attenuation_CorrectionTable.rst