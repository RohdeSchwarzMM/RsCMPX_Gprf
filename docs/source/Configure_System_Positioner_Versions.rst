Versions
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:POSitioner<PositionerIdx>:VERSions

.. code-block:: python

	[CONFigure]:SYSTem:POSitioner<PositionerIdx>:VERSions



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.Versions.VersionsCls
	:members:
	:undoc-members:
	:noindex: