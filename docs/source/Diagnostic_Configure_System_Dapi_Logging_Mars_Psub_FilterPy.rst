FilterPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:PSUB:FILTer:MNAMe
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:PSUB:FILTer:RNAMe

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:PSUB:FILTer:MNAMe
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:PSUB:FILTer:RNAMe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.Mars.Psub.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: