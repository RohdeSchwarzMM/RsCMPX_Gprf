Spath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CREate:TENVironment:SPATh

.. code-block:: python

	CREate:TENVironment:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Create.Tenvironment.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: