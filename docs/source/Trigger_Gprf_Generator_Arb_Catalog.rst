Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>[:ARB]:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>[:ARB]:CATalog:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: