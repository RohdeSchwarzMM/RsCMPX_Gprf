Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wlan.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wlan.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Wlan_Measurement_MultiEval.rst