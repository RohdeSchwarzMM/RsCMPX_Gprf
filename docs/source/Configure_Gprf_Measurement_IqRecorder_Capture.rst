Capture
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.Capture.CaptureCls
	:members:
	:undoc-members:
	:noindex: