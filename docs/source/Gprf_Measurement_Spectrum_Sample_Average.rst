Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Sample.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: