Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:Z320:ATTenuation

.. code-block:: python

	[CONFigure]:SYSTem:Z320:ATTenuation



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Z320.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: