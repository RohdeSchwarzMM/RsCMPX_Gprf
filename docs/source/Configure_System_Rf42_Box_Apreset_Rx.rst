Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:SYSTem:RF42:BOX<BoxNo>:APReset:RX

.. code-block:: python

	[CONFigure]:SYSTem:RF42:BOX<BoxNo>:APReset:RX



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Rf42.Box.Apreset.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: