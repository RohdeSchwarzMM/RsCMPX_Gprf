Positioner<Positioner>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ix1 .. Ix32
	rc = driver.configure.system.positioner.repcap_positioner_get()
	driver.configure.system.positioner.repcap_positioner_set(repcap.Positioner.Ix1)





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Positioner.PositionerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.positioner.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Positioner_HwProperties.rst
	Configure_System_Positioner_Move.rst
	Configure_System_Positioner_Moving.rst
	Configure_System_Positioner_Versions.rst