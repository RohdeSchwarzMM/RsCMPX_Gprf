AmplitudeProbDensity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:APD

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:APD



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.AmplitudeProbDensity.AmplitudeProbDensityCls
	:members:
	:undoc-members:
	:noindex: