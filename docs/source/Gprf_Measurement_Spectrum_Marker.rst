Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.gprf.measurement.spectrum.marker.repcap_marker_get()
	driver.gprf.measurement.spectrum.marker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.spectrum.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Spectrum_Marker_Npeak.rst