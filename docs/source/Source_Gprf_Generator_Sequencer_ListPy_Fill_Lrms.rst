Lrms
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:SVALue
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:INCRement
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:KEEP

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:SVALue
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:INCRement
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:LRMS:KEEP



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Fill.Lrms.LrmsCls
	:members:
	:undoc-members:
	:noindex: