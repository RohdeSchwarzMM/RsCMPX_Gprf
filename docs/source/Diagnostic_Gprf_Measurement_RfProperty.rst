RfProperty
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:TFILter

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:TFILter



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.RfPropertyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.gprf.measurement.rfProperty.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Gprf_Measurement_RfProperty_Bandpass.rst
	Diagnostic_Gprf_Measurement_RfProperty_Franges.rst
	Diagnostic_Gprf_Measurement_RfProperty_Frequency.rst
	Diagnostic_Gprf_Measurement_RfProperty_Fspan.rst
	Diagnostic_Gprf_Measurement_RfProperty_Gauss.rst
	Diagnostic_Gprf_Measurement_RfProperty_IqRecorder.rst
	Diagnostic_Gprf_Measurement_RfProperty_ListPy.rst
	Diagnostic_Gprf_Measurement_RfProperty_LoFrequency.rst
	Diagnostic_Gprf_Measurement_RfProperty_NbLevel.rst
	Diagnostic_Gprf_Measurement_RfProperty_SymbolRate.rst