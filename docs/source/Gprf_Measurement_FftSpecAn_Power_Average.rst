Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Power.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: