Catalog
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Bluetooth.rst
	Catalog_Cdma.rst
	Catalog_Gprf.rst
	Catalog_Gsm.rst
	Catalog_Lte.rst
	Catalog_LteDl.rst
	Catalog_Niot.rst
	Catalog_NrDl.rst
	Catalog_NrMmw.rst
	Catalog_NrSub.rst
	Catalog_System.rst
	Catalog_Tenvironment.rst
	Catalog_Uwb.rst
	Catalog_Wcdma.rst
	Catalog_Wlan.rst
	Catalog_Wpan.rst