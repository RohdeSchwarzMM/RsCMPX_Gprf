Talignment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:TALignment
	single: READ:GPRF:MEASurement<Instance>:IQRecorder:TALignment

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:TALignment
	READ:GPRF:MEASurement<Instance>:IQRecorder:TALignment



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqRecorder.Talignment.TalignmentCls
	:members:
	:undoc-members:
	:noindex: