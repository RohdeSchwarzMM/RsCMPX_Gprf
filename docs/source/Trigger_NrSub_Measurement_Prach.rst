Prach
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:PRACh:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.Measurement.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSub.measurement.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSub_Measurement_Prach_Catalog.rst