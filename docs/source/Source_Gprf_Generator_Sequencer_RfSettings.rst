RfSettings
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_RfSettings_Spath.rst