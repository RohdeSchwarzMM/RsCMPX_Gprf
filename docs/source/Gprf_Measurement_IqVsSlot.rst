IqVsSlot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:IQVSlot
	single: STOP:GPRF:MEASurement<Instance>:IQVSlot
	single: ABORt:GPRF:MEASurement<Instance>:IQVSlot

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:IQVSlot
	STOP:GPRF:MEASurement<Instance>:IQVSlot
	ABORt:GPRF:MEASurement<Instance>:IQVSlot



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.IqVsSlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.iqVsSlot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_IqVsSlot_FreqError.rst
	Gprf_Measurement_IqVsSlot_Icomponent.rst
	Gprf_Measurement_IqVsSlot_Level.rst
	Gprf_Measurement_IqVsSlot_OfError.rst
	Gprf_Measurement_IqVsSlot_Phase.rst
	Gprf_Measurement_IqVsSlot_Qcomponent.rst
	Gprf_Measurement_IqVsSlot_State.rst