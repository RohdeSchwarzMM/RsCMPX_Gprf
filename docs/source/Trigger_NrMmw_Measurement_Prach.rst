Prach
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRMMw:MEASurement<Instance>:PRACh:SOURce

.. code-block:: python

	TRIGger:NRMMw:MEASurement<Instance>:PRACh:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrMmw.Measurement.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrMmw.measurement.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrMmw_Measurement_Prach_Catalog.rst