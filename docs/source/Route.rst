Route
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.RouteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Bluetooth.rst
	Route_Cdma.rst
	Route_Gprf.rst
	Route_Gsm.rst
	Route_Lte.rst
	Route_LteDl.rst
	Route_Niot.rst
	Route_NrDl.rst
	Route_NrMmw.rst
	Route_NrSub.rst
	Route_Uwb.rst
	Route_Wcdma.rst
	Route_Wlan.rst
	Route_Wpan.rst