Scpi
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:MEAS:SCPI:VERSion
	single: DIAGnostic:MEAS:SCPI:HOST

.. code-block:: python

	DIAGnostic:MEAS:SCPI:VERSion
	DIAGnostic:MEAS:SCPI:HOST



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Meas.Scpi.ScpiCls
	:members:
	:undoc-members:
	:noindex: