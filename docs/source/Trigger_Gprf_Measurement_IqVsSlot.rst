IqVsSlot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:TOUT
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:SLOPe
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:MODE
	single: TRIGger:GPRF:MEASurement<Instance>:IQVSlot:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:MGAP
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:TOUT
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:THReshold
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:SLOPe
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:MODE
	TRIGger:GPRF:MEASurement<Instance>:IQVSlot:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.IqVsSlot.IqVsSlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.iqVsSlot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_IqVsSlot_Catalog.rst