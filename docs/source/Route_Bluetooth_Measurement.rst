Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Bluetooth.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.bluetooth.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Bluetooth_Measurement_Spath.rst