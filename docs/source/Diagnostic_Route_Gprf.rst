Gprf
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Route.Gprf.GprfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.route.gprf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Route_Gprf_Generator.rst
	Diagnostic_Route_Gprf_Measurement.rst