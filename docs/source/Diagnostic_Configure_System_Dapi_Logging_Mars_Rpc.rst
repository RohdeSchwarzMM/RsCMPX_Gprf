Rpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:PAYLoad
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:PAYLoad
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.Mars.Rpc.RpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.configure.system.dapi.logging.mars.rpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Configure_System_Dapi_Logging_Mars_Rpc_FilterPy.rst