NbLevel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:NBLevel

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:NBLevel



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.NbLevel.NbLevelCls
	:members:
	:undoc-members:
	:noindex: