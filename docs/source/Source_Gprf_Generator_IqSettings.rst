IqSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:IQSettings:SRATe
	single: SOURce:GPRF:GENerator<Instance>:IQSettings:TMODe
	single: SOURce:GPRF:GENerator<Instance>:IQSettings:LEVel
	single: SOURce:GPRF:GENerator<Instance>:IQSettings:PEP
	single: SOURce:GPRF:GENerator<Instance>:IQSettings:CRESt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:IQSettings:SRATe
	SOURce:GPRF:GENerator<Instance>:IQSettings:TMODe
	SOURce:GPRF:GENerator<Instance>:IQSettings:LEVel
	SOURce:GPRF:GENerator<Instance>:IQSettings:PEP
	SOURce:GPRF:GENerator<Instance>:IQSettings:CRESt



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.IqSettings.IqSettingsCls
	:members:
	:undoc-members:
	:noindex: