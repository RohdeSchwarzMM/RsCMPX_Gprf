Sall
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SALL:IQFolder
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SALL:WTFolder

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SALL:IQFolder
	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SALL:WTFolder



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Canalyzer.Sall.SallCls
	:members:
	:undoc-members:
	:noindex: