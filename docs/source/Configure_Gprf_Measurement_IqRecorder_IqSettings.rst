IqSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQSettings:SRATe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQSettings:SRATe



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.IqRecorder.IqSettings.IqSettingsCls
	:members:
	:undoc-members:
	:noindex: