Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:AVERage
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:AVERage

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:AVERage
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:AVERage
	READ:GPRF:MEASurement<Instance>:POWer:LIST:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: