Spath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:SPATh:BCSWitch

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:SPATh:BCSWitch



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Generator.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.generator.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Generator_Spath_Usage.rst