Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:WLAN:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:WLAN:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:WLAN:MEASurement<Instance>:SPATh:COUNt
	ROUTe:WLAN:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wlan.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: