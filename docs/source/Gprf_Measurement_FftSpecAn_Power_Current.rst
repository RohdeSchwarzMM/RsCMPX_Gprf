Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:POWer:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.FftSpecAn.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: