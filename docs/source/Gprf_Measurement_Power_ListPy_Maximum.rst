Maximum
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.listPy.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_ListPy_Maximum_Current.rst