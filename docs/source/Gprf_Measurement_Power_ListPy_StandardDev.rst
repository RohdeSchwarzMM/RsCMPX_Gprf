StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation
	READ:GPRF:MEASurement<Instance>:POWer:LIST:SDEViation



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: