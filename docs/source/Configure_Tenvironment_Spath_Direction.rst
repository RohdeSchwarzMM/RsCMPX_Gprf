Direction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:TENVironment:SPATh:DIRection

.. code-block:: python

	[CONFigure]:TENVironment:SPATh:DIRection



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Tenvironment.Spath.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: