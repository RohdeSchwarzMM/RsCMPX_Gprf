Level
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:LEVel
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:LEVel

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:LEVel
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:LEVel



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqVsSlot.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: