Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:MINimum
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent:MINimum
	READ:GPRF:MEASurement<Instance>:POWer:CURRent:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Current.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: