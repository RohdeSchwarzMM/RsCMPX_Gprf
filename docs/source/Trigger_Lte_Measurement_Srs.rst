Srs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:SRS:SOURce

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:SRS:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Lte.Measurement.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.lte.measurement.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Lte_Measurement_Srs_Catalog.rst