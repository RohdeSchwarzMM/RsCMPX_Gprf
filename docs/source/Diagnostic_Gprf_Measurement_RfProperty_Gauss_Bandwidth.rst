Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:GAUSs:BWIDth

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:GAUSs:BWIDth



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.Gauss.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: