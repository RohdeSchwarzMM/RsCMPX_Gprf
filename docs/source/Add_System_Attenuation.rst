Attenuation
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Add.System.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.add.system.attenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Add_System_Attenuation_CorrectionTable.rst