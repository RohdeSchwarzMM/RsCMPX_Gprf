Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:SSTop

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:SSTop



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: