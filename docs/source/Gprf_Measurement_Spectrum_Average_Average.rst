Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:AVERage:AVERage



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Average.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: