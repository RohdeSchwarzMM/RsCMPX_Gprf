Ploss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:GPRF:MEASurement<instance>:PLOSs:CNAMe

.. code-block:: python

	CATalog:GPRF:MEASurement<instance>:PLOSs:CNAMe



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Gprf.Measurement.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex: