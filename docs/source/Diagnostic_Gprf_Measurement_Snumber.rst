Snumber
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:SNUMber:BBMeas
	single: DIAGnostic:GPRF:MEASurement<Instance>:SNUMber

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:SNUMber:BBMeas
	DIAGnostic:GPRF:MEASurement<Instance>:SNUMber



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: