Sequencer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:REPetition
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:NREPetition
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:RCOunt
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:SIGNal
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:CENTry
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:UOPTions

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:REPetition
	SOURce:GPRF:GENerator<Instance>:SEQuencer:NREPetition
	SOURce:GPRF:GENerator<Instance>:SEQuencer:RCOunt
	SOURce:GPRF:GENerator<Instance>:SEQuencer:SIGNal
	SOURce:GPRF:GENerator<Instance>:SEQuencer:CENTry
	SOURce:GPRF:GENerator<Instance>:SEQuencer:UOPTions



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_Apool.rst
	Source_Gprf_Generator_Sequencer_Dtone.rst
	Source_Gprf_Generator_Sequencer_ListPy.rst
	Source_Gprf_Generator_Sequencer_Marker.rst
	Source_Gprf_Generator_Sequencer_Reliability.rst
	Source_Gprf_Generator_Sequencer_RfSettings.rst
	Source_Gprf_Generator_Sequencer_Rmarker.rst
	Source_Gprf_Generator_Sequencer_State.rst
	Source_Gprf_Generator_Sequencer_Tdd.rst
	Source_Gprf_Generator_Sequencer_Wmarker.rst