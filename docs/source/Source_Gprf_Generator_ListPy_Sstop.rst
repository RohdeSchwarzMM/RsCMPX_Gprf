Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:SSTop

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:SSTop



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: