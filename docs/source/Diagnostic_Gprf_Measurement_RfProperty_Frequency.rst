Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FREQuency

.. code-block:: python

	DIAGnostic:GPRF:MEASurement<Instance>:RFPRoperty:FREQuency



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Measurement.RfProperty.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: