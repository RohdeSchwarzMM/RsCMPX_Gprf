Add
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Trigger.Add.AddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.trigger.add.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Trigger_Add_Debug.rst