Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MODify:SYSTem:ATTenuation:CTABle[:TENVironment]

.. code-block:: python

	MODify:SYSTem:ATTenuation:CTABle[:TENVironment]



.. autoclass:: RsCMPX_Gprf.Implementations.Modify.System.Attenuation.CorrectionTable.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex: