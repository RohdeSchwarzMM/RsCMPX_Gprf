Bin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.IqRecorder.Bin.BinCls
	:members:
	:undoc-members:
	:noindex: