Control
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Control.ControlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.control.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_Control_Reboot.rst
	Configure_System_Control_Restart.rst
	Configure_System_Control_Shutdown.rst