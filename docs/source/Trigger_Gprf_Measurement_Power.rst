Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:TOUT
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:MODE
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:SLOPe
	single: TRIGger:GPRF:MEASurement<Instance>:POWer:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:POWer:MGAP
	TRIGger:GPRF:MEASurement<Instance>:POWer:TOUT
	TRIGger:GPRF:MEASurement<Instance>:POWer:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:POWer:MODE
	TRIGger:GPRF:MEASurement<Instance>:POWer:THReshold
	TRIGger:GPRF:MEASurement<Instance>:POWer:SLOPe
	TRIGger:GPRF:MEASurement<Instance>:POWer:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_Power_Catalog.rst
	Trigger_Gprf_Measurement_Power_ParameterSetList.rst