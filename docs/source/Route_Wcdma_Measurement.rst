Measurement
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wcdma.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wcdma.measurement.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Wcdma_Measurement_Spath.rst