IqRecorder
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTHreshold
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTime
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SLOPe
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:MGAP
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:THReshold
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTHreshold
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTime
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SLOPe
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Measurement.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.measurement.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Measurement_IqRecorder_Catalog.rst