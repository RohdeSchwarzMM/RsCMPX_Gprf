Itransition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Itransition.ItransitionCls
	:members:
	:undoc-members:
	:noindex: