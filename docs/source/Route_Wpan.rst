Wpan
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wpan.WpanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.wpan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Wpan_Measurement.rst