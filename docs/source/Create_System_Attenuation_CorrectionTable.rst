CorrectionTable
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Create.System.Attenuation.CorrectionTable.CorrectionTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.create.system.attenuation.correctionTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Create_System_Attenuation_CorrectionTable_Globale.rst
	Create_System_Attenuation_CorrectionTable_Tenvironment.rst