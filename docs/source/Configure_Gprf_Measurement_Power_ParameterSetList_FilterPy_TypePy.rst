TypePy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:TYPE
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:TYPE:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:TYPE
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:TYPE:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ParameterSetList.FilterPy.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: