FilterPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:FILTer:MNAMe
	single: DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:FILTer:RNAMe

.. code-block:: python

	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:FILTer:MNAMe
	DIAGnostic[:CONFigure]:SYSTem:DAPI:LOGGing:MARS:RPC:FILTer:RNAMe



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Configure.System.Dapi.Logging.Mars.Rpc.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex: