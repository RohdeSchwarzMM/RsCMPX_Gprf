Wcdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Wcdma_Measurement.rst