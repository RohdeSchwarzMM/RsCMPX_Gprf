Evaluate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<instance>:PLOSs:EVALuate

.. code-block:: python

	INITiate:GPRF:MEASurement<instance>:PLOSs:EVALuate



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Evaluate.EvaluateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.evaluate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Evaluate_Frequency.rst
	Gprf_Measurement_Ploss_Evaluate_Gain.rst
	Gprf_Measurement_Ploss_Evaluate_State.rst
	Gprf_Measurement_Ploss_Evaluate_Trace.rst