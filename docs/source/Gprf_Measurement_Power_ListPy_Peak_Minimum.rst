Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum
	READ:GPRF:MEASurement<Instance>:POWer:LIST:PEAK:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Peak.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: