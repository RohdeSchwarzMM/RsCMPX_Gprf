Tx
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Generator.Spath.Usage.Bench.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.gprf.generator.spath.usage.bench.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Gprf_Generator_Spath_Usage_Bench_Tx_Single.rst