State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:MATCh:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:MATCh:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Match.State.StateCls
	:members:
	:undoc-members:
	:noindex: