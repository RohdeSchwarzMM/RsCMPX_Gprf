ExtPwrSensor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:EPSensor
	single: STOP:GPRF:MEASurement<Instance>:EPSensor
	single: ABORt:GPRF:MEASurement<Instance>:EPSensor
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor:IDN
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor
	single: READ:GPRF:MEASurement<Instance>:EPSensor

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:EPSensor
	STOP:GPRF:MEASurement<Instance>:EPSensor
	ABORt:GPRF:MEASurement<Instance>:EPSensor
	FETCh:GPRF:MEASurement<Instance>:EPSensor:IDN
	FETCh:GPRF:MEASurement<Instance>:EPSensor
	READ:GPRF:MEASurement<Instance>:EPSensor



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.ExtPwrSensor.ExtPwrSensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.extPwrSensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_ExtPwrSensor_State.rst