Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:SAMPle:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Sample.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: