Cdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Route.Cdma.CdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.cdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Cdma_Measurement.rst