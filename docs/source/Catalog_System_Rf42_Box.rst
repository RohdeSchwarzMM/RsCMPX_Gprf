Box
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:SYSTem:RF42:BOX

.. code-block:: python

	CATalog:SYSTem:RF42:BOX



.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.System.Rf42.Box.BoxCls
	:members:
	:undoc-members:
	:noindex: