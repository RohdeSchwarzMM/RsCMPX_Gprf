Vse
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [CONFigure]:SYSTem:VSE:CONNect
	single: [CONFigure]:SYSTem:VSE:DISConnect

.. code-block:: python

	[CONFigure]:SYSTem:VSE:CONNect
	[CONFigure]:SYSTem:VSE:DISConnect



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.System.Vse.VseCls
	:members:
	:undoc-members:
	:noindex: