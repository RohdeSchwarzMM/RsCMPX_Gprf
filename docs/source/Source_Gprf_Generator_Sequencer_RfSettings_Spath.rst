Spath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:RFSettings:SPATh:CSET

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:RFSettings:SPATh:CSET



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.RfSettings.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: