Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Minimum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: