Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:SAMPles:RANGe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:SAMPles:RANGe



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Arb.Samples.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: