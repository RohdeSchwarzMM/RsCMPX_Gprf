IsMoving
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:SYSTem:POSitioner<PositionerIdx>:ISMoving

.. code-block:: python

	SENSe:SYSTem:POSitioner<PositionerIdx>:ISMoving



.. autoclass:: RsCMPX_Gprf.Implementations.Sense.System.Positioner.IsMoving.IsMovingCls
	:members:
	:undoc-members:
	:noindex: