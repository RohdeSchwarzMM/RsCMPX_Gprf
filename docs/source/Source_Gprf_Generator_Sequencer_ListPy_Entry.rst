Entry
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:DELete

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:DELete



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.ListPy.Entry.EntryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.gprf.generator.sequencer.listPy.entry.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Gprf_Generator_Sequencer_ListPy_Entry_Call.rst
	Source_Gprf_Generator_Sequencer_ListPy_Entry_Insert.rst
	Source_Gprf_Generator_Sequencer_ListPy_Entry_Mdown.rst
	Source_Gprf_Generator_Sequencer_ListPy_Entry_Mup.rst