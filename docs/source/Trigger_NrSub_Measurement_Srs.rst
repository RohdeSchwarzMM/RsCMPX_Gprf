Srs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:SRS:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.NrSub.Measurement.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSub.measurement.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSub_Measurement_Srs_Catalog.rst