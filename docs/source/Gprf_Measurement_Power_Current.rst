Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.power.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Power_Current_Maximum.rst
	Gprf_Measurement_Power_Current_Minimum.rst
	Gprf_Measurement_Power_Current_Rms.rst