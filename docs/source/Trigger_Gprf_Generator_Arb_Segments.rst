Segments
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MODE

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MODE



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gprf.generator.arb.segments.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gprf_Generator_Arb_Segments_Manual.rst