Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:WPAN:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:WPAN:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:WPAN:MEASurement<Instance>:SPATh:COUNt
	ROUTe:WPAN:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Wpan.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: