Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MAXimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Spectrum.Rms.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: