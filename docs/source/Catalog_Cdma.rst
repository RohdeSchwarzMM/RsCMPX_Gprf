Cdma
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Catalog.Cdma.CdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.cdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Cdma_Measurement.rst