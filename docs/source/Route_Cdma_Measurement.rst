Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:CDMA:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:CDMA:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Cdma.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: