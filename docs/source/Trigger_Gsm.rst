Gsm
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gsm.GsmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.gsm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Gsm_Measurement.rst