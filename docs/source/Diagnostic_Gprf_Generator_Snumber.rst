Snumber
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:SNUMber:BBGenerator
	single: DIAGnostic:GPRF:GENerator<Instance>:SNUMber

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:SNUMber:BBGenerator
	DIAGnostic:GPRF:GENerator<Instance>:SNUMber



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: