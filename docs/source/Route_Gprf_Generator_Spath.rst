Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:GPRF:GENerator<Instance>:SPATh:COUNt
	single: ROUTe:GPRF:GENerator<Instance>:SPATh

.. code-block:: python

	ROUTe:GPRF:GENerator<Instance>:SPATh:COUNt
	ROUTe:GPRF:GENerator<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.Gprf.Generator.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.gprf.generator.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Gprf_Generator_Spath_Group.rst