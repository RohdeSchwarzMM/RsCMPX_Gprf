Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:MANual:EXECute



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Gprf.Generator.Arb.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: