Auto
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AUTO:MTIMe
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:AUTO:NSR

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AUTO:MTIMe
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:AUTO:NSR



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.ExtPwrSensor.Auto.AutoCls
	:members:
	:undoc-members:
	:noindex: