Tpc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WCDMa:MEASurement<Instance>:TPC:SOURce

.. code-block:: python

	TRIGger:WCDMa:MEASurement<Instance>:TPC:SOURce



.. autoclass:: RsCMPX_Gprf.Implementations.Trigger.Wcdma.Measurement.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.wcdma.measurement.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Wcdma_Measurement_Tpc_Catalog.rst