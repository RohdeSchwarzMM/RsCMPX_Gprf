Spath
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:NRMMw:MEASurement<Instance>:SPATh:COUNt
	single: ROUTe:NRMMw:MEASurement<Instance>:SPATh

.. code-block:: python

	ROUTe:NRMMw:MEASurement<Instance>:SPATh:COUNt
	ROUTe:NRMMw:MEASurement<Instance>:SPATh



.. autoclass:: RsCMPX_Gprf.Implementations.Route.NrMmw.Measurement.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex: