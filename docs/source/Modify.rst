Modify
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.Modify.ModifyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.modify.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Modify_System.rst