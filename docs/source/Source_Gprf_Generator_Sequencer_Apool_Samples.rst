Samples
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Apool.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex: