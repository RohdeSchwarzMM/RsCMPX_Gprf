Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum
	single: FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum
	single: READ:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum
	FETCh:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum
	READ:GPRF:MEASurement<Instance>:POWer:PEAK:MINimum



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.Peak.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: