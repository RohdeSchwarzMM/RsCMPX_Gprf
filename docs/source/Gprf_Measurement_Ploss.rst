Ploss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:GPRF:MEASurement<Instance>:PLOSs
	single: ABORt:GPRF:MEASurement<Instance>:PLOSs

.. code-block:: python

	STOP:GPRF:MEASurement<Instance>:PLOSs
	ABORt:GPRF:MEASurement<Instance>:PLOSs



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.gprf.measurement.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Gprf_Measurement_Ploss_Clear.rst
	Gprf_Measurement_Ploss_Eeprom.rst
	Gprf_Measurement_Ploss_Evaluate.rst
	Gprf_Measurement_Ploss_Match.rst
	Gprf_Measurement_Ploss_Open.rst
	Gprf_Measurement_Ploss_Short.rst
	Gprf_Measurement_Ploss_State.rst