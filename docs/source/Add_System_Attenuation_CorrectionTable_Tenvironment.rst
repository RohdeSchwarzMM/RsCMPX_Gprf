Tenvironment
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ADD:SYSTem:ATTenuation:CTABle[:TENVironment]

.. code-block:: python

	ADD:SYSTem:ATTenuation:CTABle[:TENVironment]



.. autoclass:: RsCMPX_Gprf.Implementations.Add.System.Attenuation.CorrectionTable.Tenvironment.TenvironmentCls
	:members:
	:undoc-members:
	:noindex: