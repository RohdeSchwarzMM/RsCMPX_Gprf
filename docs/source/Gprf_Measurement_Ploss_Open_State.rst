State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<instance>:PLOSs:OPEN:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<instance>:PLOSs:OPEN:STATe



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Ploss.Open.State.StateCls
	:members:
	:undoc-members:
	:noindex: