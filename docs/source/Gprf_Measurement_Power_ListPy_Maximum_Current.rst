Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:LIST:MAXimum:CURRent



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Power.ListPy.Maximum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: