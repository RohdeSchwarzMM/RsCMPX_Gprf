Connection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:IDX<idx>:CATalog:CONNection

.. code-block:: python

	[CONFigure]:GPRF:MEASurement<instance>:POWer:LIST:IDX<idx>:CATalog:CONNection



.. autoclass:: RsCMPX_Gprf.Implementations.Configure.Gprf.Measurement.Power.ListPy.Idx.Catalog.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex: