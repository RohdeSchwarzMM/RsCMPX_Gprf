RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:GPRF:GENerator<Instance>:RFSettings:NSMargin

.. code-block:: python

	DIAGnostic:GPRF:GENerator<Instance>:RFSettings:NSMargin



.. autoclass:: RsCMPX_Gprf.Implementations.Diagnostic.Gprf.Generator.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: