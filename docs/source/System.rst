System
----------------------------------------





.. autoclass:: RsCMPX_Gprf.Implementations.System.SystemCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Attenuation.rst
	System_Date.rst
	System_Time.rst