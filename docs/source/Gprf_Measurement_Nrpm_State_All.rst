All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRPM:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRPM:STATe:ALL



.. autoclass:: RsCMPX_Gprf.Implementations.Gprf.Measurement.Nrpm.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: