Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:TDD:MODE

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:TDD:MODE



.. autoclass:: RsCMPX_Gprf.Implementations.Source.Gprf.Generator.Sequencer.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: